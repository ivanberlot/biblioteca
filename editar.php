<?php
/**
 *
 * @author iberlot <@> ivanberlot@gmail.com
 * @todo FechaC 23/12/2015 - Lenguaje PHP
 *
 * @name editar.php
 *
 * @version 0.1 - Version de inicio
 *
 * @package Mytthos
 *
 * @category config
 *
 * @link Config/includes - Archivo con todos los includes del sistema
 *
 */
/*
 * Querido programador:
 * Cuando escribi este codigo, solo Dios y yo sabiamos como funcionaba.
 * Ahora, Solo Dios lo sabe!!!
 * Asi que, si esta tratando de 'optimizar' esta rutina y fracasa (seguramente),
 * por favor, incremente el siguiente contador como una advertencia para el
 * siguiente colega:
 * totalHorasPerdidasAqui = 5
 */
require_once ("config/includes.php");

if (isset ($_REQUEST ["idCapitulo"]))
{
	$idCapitulo = $_REQUEST ['idCapitulo'];

	echo $idCapitulo;

	$sql = "SELECT
            	    Capitulo.idCapitulo idCapitulo,
            	    Capitulo.nrOrden nrOrden,
            	    Capitulo.titulo tituloCap,
            	    Capitulo.Archivo archivo,
					Capitulo.ArchivoOtro otro,
            	    Libro.idLibro idLibro,
            	    Libro.ordenSaga ordenSaga,
            	    Libro.titulo tituloLibro,
            	    Saga.idSaga idSaga,
            	    Saga.titulo tituloSaga,
            	    Autor.idAutor idAutor,
            	    Autor.apellido apellido,
            	    Autor.nombre nombre,
            	    Autor.segNombre segNombre
            	FROM
            	    Capitulo,
            	    Autor,
            	    Libro,
            	    Saga
            	WHERE
            	    Capitulo.idLibro = Libro.idLibro
            	    AND Libro.idSaga = Saga.idSaga
            	    AND Saga.idAutor = Autor.idAutor
            	    AND Capitulo.idCapitulo = " . $idCapitulo;

	$result = mysqli_query ($link, $sql) or die ('Query error: ' . mysqli_error ($link));

	$row = mysqli_fetch_array ($result, MYSQLI_ASSOC);

	$idCapitulo = $row ['idCapitulo'];

	$apellido = trim ($row ['apellido']);
	$realname = trim ($row ['nombre']);
	$segname = trim ($row ['segNombre']);

	if ($segname != "")
	{
		$direCarpeta = $apellido . "_" . $realname . "_" . $segname;
	}
	else
	{
		$direCarpeta = $apellido . "_" . $realname;
	}

	$tituloCarpetaSaga = str_replace (' ', '_', trim ($row ['tituloSaga']));

	$tituloCarpeta = str_replace (' ', '_', trim ($row ['tituloLibro']));

	$tituloCarpeta = $direCarpeta . "/" . $tituloCarpetaSaga . "/" . str_pad ($row ['ordenSaga'], 2, "0", STR_PAD_LEFT) . "-" . $tituloCarpeta;

	$capitulo = $row ['archivo'];

	$archivo = "Biblioteca/" . $tituloCarpeta . "/Capitulos/" . $capitulo . ".txt";
}

if (isset ($_POST ["submit"]))
{
	if ($fp = fopen ($archivo, "w"))
	{
		if (mb_detect_encoding ($_POST ["newdata"]) == 'ASCII')
		{
			$newdata = utf8_decode ($_POST ["newdata"]);
		}
		else
		{
			$newdata = $_POST ["newdata"];
		}
		// $newdata = utf8_encode ($_POST["newdata"]);
		fwrite ($fp, stripslashes ($newdata));
		fclose ($fp);
	}
}
?>

<body>
    <Div id="cuerpo">
        <fieldset>
            <legend>Editar "<?php

												echo $row ['tituloLibro'] . " - " . $row ['tituloCap'];
												?>"</legend>
            <form action="<?php

												echo $_SERVER ["PHP_SELF"];
												?>" method="post">
                <textarea name="newdata" rows="50" cols="100"><?php
																// system ('iconv -f ascii -t utf8 '.$archivo, $pehp);
																// print_r('iconv -f ascii -t utf8 '.$archivo);
																// if($archivo!=""){
																// exec("iconv -f ISO-8859-1 -t UTF-8 ".$archivo." > ".$archivo.".UTF8");
																// }

																$ar = fopen ($archivo, "r") or die ("No se pudo abrir el archivo");
																while (! feof ($ar))
																{
																	$linea = fgets ($ar); // or die("No se pudo obtener la linea"); //Obtiene una l�nea desde el puntero a un fichero
																	$lineasalto = $linea;
																	// echo mb_detect_encoding ($lineasalto);
																	// echo utf8_encode ($lineasalto); // Le indicamos que convierta el texto a utf8 para que reconzca acentos y �'s

																	// if (mb_detect_encoding ($lineasalto) == 'UTF-8')
																	// {
																	// $lineasalto = utf8_encode ($lineasalto);
																	// }
																	if (mb_detect_encoding ($lineasalto) == 'ASCII')
																	{
																		$lineasalto = utf8_decode ($lineasalto);
																	}
																	echo $lineasalto;
																}
																fclose ($ar);
																?>
			</textarea>
                <input type='text' name='idCapitulo' value='<?php

																echo $idCapitulo;
																?>' style='display: none' />
                <Br />
                <Br />
                <input type="submit" name="submit" value="Guardar">
            </form>
            <a href='subirAudio.php?idCapitulo=<?php

												echo $idCapitulo;
												?>' title='Subir audio' target="_blank"><i class='fa fa-music' aria-hidden='true'></i></a>

<?php
$sql = "SELECT idCapitulo FROM Capitulo WHERE idLibro = :idlibro AND nrOrden = :orden";

$parametros = array ();
$parametros [0] = $row ['idLibro'];
$parametros [1] = $row ['nrOrden'] - 1;

$html = "<Br /><Br /><Br /><Br />";

if ($result = $db->query ($sql, $esParam = true, $parametros))
{
	if ($rows = $db->fetch_array ($result))
	{
		$html .= "<a href='editar.php?idCapitulo=" . $rows ['idCapitulo'] . "' accesskey='Left arrow'>Anterior</a>    ";
	}
}

$parametros [1] = $row ['nrOrden'] + 1;

if ($result = $db->query ($sql, $esParam = true, $parametros))
{
	if ($rows = $db->fetch_array ($result))
	{
		$html .= "     <a href='editar.php?idCapitulo=" . $rows ['idCapitulo'] . "' accesskey='z'>Siguiente</a>";
	}
}

echo $html;
?>

        </fieldset>
    </Div>
</body>
</html>