<?php
session_start ();
/*
 * Login de Requerimientos
 * mixed by Marcexl
 * version 24112015
 *
 * - Codigo para conectarse a las apis de google
 */

/* traigo los includes */
include_once ("config/config.php");
include_once ("config/variables.php");
// include_once ("config/conexion.php");

if (isset ($_SESSION['cuenta']))
{
	header ("Location:/index.php");
	exit ();
}

if (isset ($_GET['id']))
{
	$app = $_GET['id'];
	$_SESSION['app'] = $app;
}

require_once ("/var/www/html/google-api-php-client/src/Google/autoload.php");
ob_start ();

/**
 * **********************************************
 * Make an API request on behalf of a user.
 * In
 * this case we need to have a valid OAuth 2.0
 * token for the user, so we need to send them
 * through a login flow. To do this we need some
 * information from our API console project.
 * **********************************************
 */
$client = new Google_Client ();
$client->setClientId ($client_id);
$client->setClientSecret ($client_secret);
$client->setRedirectUri ($redirect_uri);
// $client->addScope("https://www.googleapis.com/auth/userinfo.profile"); Con este scope solo no te trae el mail
// Con todos estos si.
$client->setScopes (array (
		"https://www.googleapis.com/auth/plus.login",
		"https://www.googleapis.com/auth/userinfo.email",
		"https://www.googleapis.com/auth/userinfo.profile",
		"https://www.googleapis.com/auth/plus.me",
		"https://www.googleapis.com/auth/admin.directory.user"
));

/**
 * **********************************************
 * Se crea el objeto $oauth2Service
 * **********************************************
 */
$oauth2Service = new Google_Service_Oauth2 ($client);

/**
 * **********************************************
 * Se realiza la solicitud de un acces token
 * si es que no esta la sesion iniciada
 * **********************************************
 */
if (isset ($_REQUEST['logout']))
{
	unset ($_SESSION['access_token']);
}

if (isset ($_GET['code']))
{
	$client->authenticate ($_GET['code']);
	$_SESSION['access_token'] = $client->getAccessToken ();
	$redirect = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
	header ('Location: ' . filter_var ($redirect, FILTER_SANITIZE_URL));
}
/**
 * **********************************************
 * Una vez oauth te devuelve un token lo guardamos
 * como variable de session.
 * **********************************************
 */

if (isset ($_SESSION['access_token']) && $_SESSION['access_token'])
{
	$client->setAccessToken ($_SESSION['access_token']);
}
else
{
	$authUrl = $client->createAuthUrl ();
}

/**
 * **********************************************
 * Aca comienza el codigo de la pagina para
 * logearse
 * ***********************************************
 */

if (isset ($authUrl))
{

	// include ("inc/header.php");

	?>
<style>
#login-nav{
	display: none;
}
</style>
<section id="content">
	<h1 align="center">Ingreso al Sistema</h1>
	<div class="row-login">
		<fieldset>
			<p align="center"><a class="greenBut" href="<?php

	echo $authUrl?>"><i class="fa fa-sign-in"></i> Ingresar</a></p>
			<p>&nbsp;</p>


			<?php

	if (isset ($_GET['id']))
	{

		$id = $_GET['id'];

		if ($id == 0)
		{
			echo '<p align="center"><a href="/gestionAcademica/solicitud-correo-electronico" class="linkLogin"><i class="fa fa-openid"></i> Registrarse aqu&iacute;</a></p>';
		}
		else
		{
			echo '<p align="center"><a class="greenBut" href="https://servicios.usal.edu.ar/novus/Main/Postulacion.html"><i class="fa fa-sign-in"></i> Postularse</a></p>';

			echo '<p align="center"><a href="https://servicios.usal.edu.ar/NOVUS/Instructivo%20para%20la%20carga%20de%20Datos%20de%20Postulantes1.3.pdf
" class="linkLogin"><i class="fa fa-file"></i> Ver Instructivo</a></p>';
		}
	}

	?>


			<p align="center"><a href="/cgi-bin/Login/password.php" class="linkLogin"><i class="fa fa-key"></i> Olvide mi contrase&ntilde;a</a></p>

<!-- 			<h3>Aclaraciones:</h3> -->
<!-- 			<ul class="listElements"> -->
<!-- 				<li>Utilizar el correo acad&eacute;mico para ingresar al sistema. El usuario es la direcci&oacute;n de mail completa, incluyendo el @usal.edu.ar.</li> -->
<!-- 				<li>La clave de acceso es la misma que se utiliza para acceder al correo acad&eacute;mico brindado por la Universidad.</li> -->
<!-- 			</ul> -->

<!-- 			<h3>Mesa de ayuda:</h3> -->
<!-- 			<p>Ante cualquier consulta comunicarse telef&oacute;nicamente al +54 (011) 4812-4588 (l&iacute;neas rotativas) de Lunes a Viernes de 9 a 18hs o por mail a servicios@usal.edu.ar.</p> -->

		 </fieldset>
	 </div><!--end of cuerpo-->
 </section><!--end of content-->

<div class="separador"></div>

<?php
}
else
{
	include ("/web/html/inc/header.php");
	/**
	 * *************************
	 * Aca ya ingresamos!!!
	 * **************************
	 */

	/**
	 * ******Api login***********
	 */

	$oauth2Service = new Google_Service_Oauth2 ($client);
	$userinfo = $oauth2Service->userinfo;
	$getUser_info = $userinfo->get ();

	$email = $getUser_info['email'];
	$nombreApellido = $getUser_info['name'];
	$foto = $getUser_info['picture'];

	/* primero nos fijamos si tiene @usal.edu.ar */

	// $haveit = '@usal.edu.ar';
	/*
	 * $pos = strpos ($email, $haveit);
	 *
	 * if ($pos == false)
	 * {
	 *
	 * echo '<div id="content">
	 * <div id="separadorh"></div>
	 * <h3><?php echo $Titulo;?></h3>
	 * <div id="separadorh"></div>
	 * <div id="cuerpo" align="center">
	 * <div id="alert">
	 * <div class="alertHeader">Ha ocurrido un error</div>
	 * <p>Para acceder debe tener una cuenta @usal.edu.ar<br> <a href="/gestionAcademica/solicitud-correo-electronico" class="linkLogin">Registrarse aqu&iacute;</a></p>
	 * <p><a href="salir.php" class="mxlbutton">Salir</a></p>
	 * </div>
	 * </div>
	 * </div>';
	 * exit ();
	 * }
	 *
	 * /* si es valida eliminamos el @usal.edu.ar
	 */

	$cortar = '@';
	$pos2 = strpos ($email, $cortar);
	$cuenta = substr ($email, 0, $pos2);

	/* generamos el password general */
	$pass = '102024ce';

	/* guardo las variables de session */
	$token = json_decode ($_SESSION['access_token']);
	$_SESSION['token'] = $token->{'access_token'};
	$_SESSION['validate'] = $_SESSION['token'];
	$_SESSION['cuenta'] = $cuenta;
	$_SESSION['mailUsuario'] = $email;
	$_SESSION['foto'] = $foto;
	$_SESSION['estado'] = 'Iniciada';

	header ("Location:config/validar_usuario.php");
	exit ();
}
?>

<?php
// include("/web/html/inc/informacion.php"); #incluimos el footer
// include("/web/html/inc/sitios.php"); #incluimos el footer
// include("/web/html/inc/footer.php"); #incluimos el footer
ob_end_flush ();
?>
<!-- <script type="text/javascript" src="/js/footer.js"></script> -->
