<?php

/**
 * Listado con todas las funciones utilizadas por el sistema
 *
 * @author iberlot <@> ivanberlot@gmail.com
 * @version 0.2
 *
 *
 */
/**
 * Verifica la codificacion de un texto.
 *
 * @param String $texto
 * @return string
 */
function codificacion($texto)
{
	$c = 0;
	$ascii = true;
	for($i = 0; $i < strlen ($texto); $i ++)
	{
		$byte = ord ($texto [$i]);
		if ($c > 0)
		{
			if (($byte >> 6) != 0x2)
			{
				return ISO_8859_1;
			}
			else
			{
				$c --;
			}
		}
		elseif ($byte & 0x80)
		{
			$ascii = false;
			if (($byte >> 5) == 0x6)
			{
				$c = 1;
			}
			elseif (($byte >> 4) == 0xE)
			{
				$c = 2;
			}
			elseif (($byte >> 3) == 0x1E)
			{
				$c = 3;
			}
			else
			{
				return ISO_8859_1;
			}
		}
	}
	return ($ascii) ? ASCII : UTF_8;
}

/**
 * Recodifica un texto con utf8_encode siempre que su codificacion sea ISO_8859_1
 *
 * @param String $texto
 * @return string
 */
function utf8_encode_seguro($texto)
{
	return (codificacion ($texto) == ISO_8859_1) ? utf8_encode ($texto) : $texto;
}

/**
 * Recupera todos los datos de un libro en base a los parametros pasados.
 *
 * @param object $db
 *        	conector a la base de datos
 * @param array $param
 *        	parametros con los que realizar la busqueda
 * @return string[]
 */
function get_Libro($db, $param)
{
	$where = array ();
	$parametros = array ();

	if (isset ($param ['resenia']) and $param ['resenia'] != "")
	{
		$where [] = " resenia = :resenia ";

		$parametros [] = $param ['resenia'];
	}
	if (isset ($param ['anio']) and $param ['anio'] != "")
	{
		$where [] = " anio = :anio ";

		$parametros [] = $param ['anio'];
	}
	if (isset ($param ['idSaga']) and $param ['idSaga'] != "")
	{
		$where [] = " idSaga = :idSaga ";

		$parametros [] = $param ['idSaga'];
	}
	if (isset ($param ['titulo']) and $param ['titulo'] != "")
	{
		$where [] = " titulo = :titulo ";

		$parametros [] = $param ['titulo'];
	}
	if (isset ($param ['idAutor']) and $param ['idAutor'] != "")
	{
		$where [] = " idAutor = :idAutor ";

		$parametros [] = $param ['idAutor'];
	}
	if (isset ($param ['idLibro']) and $param ['idLibro'] != "")
	{
		$where [] = " idLibro = :idLibro ";

		$parametros [] = $param ['idLibro'];
	}
	if (isset ($param ['ordenSaga']) and $param ['ordenSaga'] != "")
	{
		$where [] = " ordenSaga = :ordenSaga ";

		$parametros [] = $param ['ordenSaga'];
	}
	if (isset ($param ['imagen']) and $param ['imagen'] != "")
	{
		$where [] = " imagen = :imagen ";

		$parametros [] = $param ['imagen'];
	}

	if ($where != "")
	{
		$where = implode (" AND ", $where);

		$where = "AND " . $where;
	}
	else
	{
		$where = "";
	}

	$sql = "SELECT * FROM Libro WHERE 1=1 " . $where;

	$result = $db->query ($sql, $esParam = true, $parametros);

	$datos = $db->fetch_array ($result);

	if (isset ($datos) and $datos != "")
	{
		return $datos;
	}
}

/**
 * Recupera todos los datos de una saga en base a los parametros pasados.
 *
 * @param object $db
 *        	conector a la base de datos
 * @param array $param
 *        	parametros con los que realizar la busqueda
 * @return string[]
 */
function get_Saga($db, $param)
{
	$where = array ();
	$parametros = array ();

	if (isset ($param ['idSaga']) and $param ['idSaga'] != "")
	{
		$where [] = " idSaga = :idSaga ";

		$parametros [] = $param ['idSaga'];
	}
	if (isset ($param ['titulo']) and $param ['titulo'] != "")
	{
		$where [] = " titulo = :titulo ";

		$parametros [] = $param ['titulo'];
	}
	if (isset ($param ['idAutor']) and $param ['idAutor'] != "")
	{
		$where [] = " idAutor = :idAutor ";

		$parametros [] = $param ['idAutor'];
	}
	if (isset ($param ['cantLibros']) and $param ['cantLibros'] != "")
	{
		$where [] = " cantLibros = :cantLibros ";

		$parametros [] = $param ['cantLibros'];
	}
	if (isset ($param ['descripcion']) and $param ['descripcion'] != "")
	{
		$where [] = " descripcion = :descripcion ";

		$parametros [] = $param ['descripcion'];
	}
	if (isset ($param ['imagen']) and $param ['imagen'] != "")
	{
		$where [] = " imagen = :imagen ";

		$parametros [] = $param ['imagen'];
	}

	if ($where != "")
	{
		$where = implode (" AND ", $where);

		$where = "AND " . $where;
	}
	else
	{
		$where = "";
	}

	$sql = "SELECT * FROM Saga WHERE 1=1 " . $where;

	$result = $db->query ($sql, $esParam = true, $parametros);

	$datos = $db->fetch_array ($result);

	if (isset ($datos) and $datos != "")
	{
		return $datos;
	}
}

/**
 * Recupera todos los datos de un autor en base a los parametros pasados.
 *
 * @param object $db
 *        	conector a la base de datos
 * @param array $param
 *        	parametros con los que realizar la busqueda
 * @return string[]
 */
function get_Autor($db, $param)
{
	$where = array ();
	$parametros = array ();

	// FIXME - Quedan agregar `fechaNac``fechaDec` que como son fechas se deja para mas tarde

	if (isset ($param ['apellido']) and $param ['apellido'] != "")
	{
		$where [] = " apellido = :apellido ";

		$parametros [] = $param ['apellido'];
	}
	if (isset ($param ['nombre']) and $param ['nombre'] != "")
	{
		$where [] = " nombre = :nombre ";

		$parametros [] = $param ['nombre'];
	}
	if (isset ($param ['idAutor']) and $param ['idAutor'] != "")
	{
		$where [] = " idAutor = :idAutor ";

		$parametros [] = $param ['idAutor'];
	}
	if (isset ($param ['segNombre']) and $param ['segNombre'] != "")
	{
		$where [] = " segNombre = :segNombre ";

		$parametros [] = $param ['segNombre'];
	}
	if (isset ($param ['nacionalidad']) and $param ['nacionalidad'] != "")
	{
		$where [] = " nacionalidad = :nacionalidad ";

		$parametros [] = $param ['nacionalidad'];
	}
	if (isset ($param ['directorio']) and $param ['directorio'] != "")
	{
		$where [] = " directorio = :directorio ";

		$parametros [] = $param ['directorio'];
	}

	if ($where != "")
	{
		$where = implode (" AND ", $where);

		$where = "AND " . $where;
	}
	else
	{
		$where = "";
	}

	$sql = "SELECT * FROM Autor WHERE 1=1 " . $where;

	$result = $db->query ($sql, $esParam = true, $parametros);

	$datos = $db->fetch_array ($result);

	if (isset ($datos) and $datos != "")
	{
		return $datos;
	}
}

/**
 *
 * @param object $db
 *        	conector a la base de datos
 * @param array $param
 *        	parametros con los que realizar la busqueda - idCapitulo
 * @return string[]
 */
function get_Archivo_capitulo($db, $param)
{
	$where = array ();
	$parametros = array ();

	if (isset ($param ["idCapitulo"]) and $param ["idCapitulo"] != "")
	{
		$where [] = " Capitulo.idCapitulo = :idCapitulo ";
		$parametros [] = trim ($param ["idCapitulo"]);
	}
	else
	{
	}

	if ($where != "")
	{
		$where = implode (" AND ", $where);

		$where = "AND " . $where;
	}
	else
	{
		$where = "";
	}

	$sql = "SELECT
					Capitulo.idCapitulo idCapitulo,
					Capitulo.nrOrden nrOrden,
					Capitulo.titulo tituloCap,
					Capitulo.Archivo archivo,
					Capitulo.ArchivoOtro otro,
					Capitulo.codigoYouTube codigoYouTube,
					Libro.idLibro idLibro,
					Libro.ordenSaga ordenSaga,
					Libro.titulo tituloLibro,
					Saga.idSaga idSaga,
					Saga.titulo tituloSaga,
					Autor.idAutor idAutor
				FROM
					Capitulo,
					Autor,
					Libro,
					Saga
				WHERE
					Capitulo.idLibro = Libro.idLibro
					AND Libro.idSaga = Saga.idSaga
					AND Saga.idAutor = Autor.idAutor
					" . $where;

	$result = $db->query ($sql, true, $parametros);

	if ($row = $db->fetch_array ($result))
	{

		$direAutor = getArrayPosNobresAutor ($db, $row ['idAutor']);

		$direCarpeta = $direAutor [0];

		$tituloCarpetaSaga = str_replace (' ', '_', trim ($row ['tituloSaga']));

		$tituloCarpeta = str_replace (' ', '_', trim ($row ['tituloLibro']));

		$tituloLibro = str_pad ($row ['ordenSaga'], 2, "0", STR_PAD_LEFT) . "-" . $tituloCarpeta;

		$tituloCarpeta = $direCarpeta . "/" . $tituloCarpetaSaga . "/" . $tituloLibro;

		$capitulo = $row ['archivo'];

		$datos = array ();
		// $archivo = "Biblioteca/" . $tituloCarpeta . "/Capitulos/" . $tituloLibro . "-" . $capitulo . ".txt";
		$datos ['archivo'] = "Biblioteca/" . $tituloCarpeta . "/Capitulos/" . $capitulo . ".txt";
		$datos ['archivoAudio'] = "Biblioteca/" . $tituloCarpeta . "/Audios/" . $capitulo . ".mp3";
		$datos ['dirLibro'] = "Biblioteca/" . $tituloCarpeta;
		// $codigoYouTube = $row['codigoYouTube'];
	}

	if (isset ($datos) and $datos != "")
	{
		return $datos;
	}
}

/**
 *
 * @param object $db
 *        	conector a la base de datos
 * @param array $param
 *        	parametros con los que realizar la busqueda
 * @return string[]
 */
function get_Directorio($db, $param)
{
	$join = array ();
	$campos = array ();
	$where = array ();
	$parametros = array ();
	$capitulo = false;
	$libro = false;
	$saga = false;

	if (isset ($param ["idAutor"]) and $param ["idAutor"] != "")
	{
		$where [] = " Autor.idAutor = :idAutor ";
		$parametros [] = trim ($param ["idAutor"]);
	}

	if (isset ($param ["idLibro"]) and $param ["idLibro"] != "")
	{
		$join [] = " INNER JOIN Libro ON Autor.idAutor = Libro.idAutor ";
		$where [] = " Libro.idLibro = :idLibro ";
		$parametros [] = trim ($param ["idLibro"]);

		$campos [] = " Libro.idLibro idLibro ";
		$campos [] = " Libro.ordenSaga ordenSaga ";
		$campos [] = " Libro.titulo tituloLibro ";

		$libro = true;
	}
	elseif (isset ($param ["idCapitulo"]) and $param ["idCapitulo"] != "")
	{
		$join [] = " INNER JOIN Libro ON Autor.idAutor = Libro.idAutor ";
		$where [] = " Libro.idLibro=Capitulo.idLibro ";

		$campos [] = " Libro.idLibro idLibro ";
		$campos [] = " Libro.ordenSaga ordenSaga ";
		$campos [] = " Libro.titulo tituloLibro ";

		$libro = true;
	}

	if (isset ($param ["idCapitulo"]) and $param ["idCapitulo"] != "")
	{
		$join [] = " INNER JOIN Capitulo ON Capitulo.idLibro = Libro.idLibro ";
		$where [] = " Capitulo.idCapitulo = :idCapitulo ";
		$parametros [] = trim ($param ["idCapitulo"]);

		$campos [] = " Capitulo.idCapitulo idCapitulo ";
		$campos [] = " Capitulo.nrOrden nrOrden ";
		$campos [] = " Capitulo.titulo tituloCap ";
		$campos [] = " Capitulo.Archivo archivo ";
		$campos [] = " Capitulo.ArchivoOtro otro ";
		$campos [] = " Capitulo.codigoYouTube codigoYouTube ";

		$capitulo = true;
	}

	if (isset ($param ["idSaga"]) and $param ["idSaga"] != "")
	{
		$join [] = " INNER JOIN Saga ON Autor.idAutor = Saga.idAutor ";
		$where [] = " Saga.idSaga = :idSaga ";
		$parametros [] = trim ($param ["idSaga"]);

		$campos [] = " Saga.idSaga idSaga ";
		$campos [] = " Saga.titulo tituloSaga ";

		$saga = true;
	}
	elseif ($libro == true)
	{
		$join [] = " INNER JOIN Saga ON Autor.idAutor = Saga.idAutor AND Libro.idSaga = Saga.idSaga";

		$campos [] = " Saga.idSaga idSaga ";
		$campos [] = " Saga.titulo tituloSaga ";

		$saga = true;
	}

	if ($where != "")
	{
		$where = implode (" AND ", $where);

		$where = "AND " . $where;
	}
	else
	{
		$where = "";
	}

	if ($join != "")
	{
		$join = implode (" ", $join);

		$join = " " . $join;
	}
	else
	{
		$join = "";
	}

	if (! empty ($campos))
	{
		$campos = implode (" , ", $campos);

		$campos = ", " . $campos;
	}
	else
	{
		$campos = "";
	}

	$sql = "SELECT
					Autor.idAutor idAutor,
					Autor.apellido apellido,
					Autor.nombre nombre,
					Autor.segNombre segNombre
					$campos
				FROM
					Autor
					$join
				WHERE 1=1 " . $where;

	$result = $db->query ($sql, true, $parametros);

	if ($row = $db->fetch_array ($result))
	{
		$apellido = trim ($row ['apellido']);
		$realname = trim ($row ['nombre']);
		$segname = trim ($row ['segNombre']);

		$datos ['idAutor'] = $row ['idAutor'];

		if ($segname != "")
		{
			$datos ['archivo'] = $datos ['directorio'] = $direCarpeta = $apellido . "_" . $realname . "_" . $segname;
		}
		else
		{
			$datos ['archivo'] = $datos ['directorio'] = $direCarpeta = $apellido . "_" . $realname;
		}

		if ($saga == true)
		{
			$datos ['idSaga'] = $row ['idSaga'];
			$datos ['archivo'] = $tituloCarpetaSaga = str_replace (' ', '_', trim ($row ['tituloSaga']));

			if ($libro == true)
			{
				$datos ['idLibro'] = $row ['idLibro'];
				$datos ['directorio'] = $datos ['directorio'] . "/" . $tituloCarpetaSaga;
				$tituloCarpeta = str_replace (' ', '_', trim ($row ['tituloLibro']));
				$datos ['archivo'] = $tituloLibro = str_pad ($row ['ordenSaga'], 2, "0", STR_PAD_LEFT) . "-" . $tituloCarpeta;

				// $tituloCarpeta = $direCarpeta . "/" . $tituloCarpetaSaga . "/" . $tituloLibro;

				if ($capitulo == true)
				{
					$datos ['idCapitulo'] = $row ['idCapitulo'];

					$datos ['directorio'] = $datos ['directorio'] . "/" . $tituloLibro;

					$capitulo = $row ['archivo'];

					// $archivo = "Biblioteca/" . $tituloCarpeta . "/Capitulos/" . $tituloLibro . "-" . $capitulo . ".txt";
					$datos ['archivo'] = "Biblioteca/" . $datos ['directorio'] . "/Capitulos/" . $capitulo . ".txt";
					$datos ['archivoAudio'] = "Biblioteca/" . $datos ['directorio'] . "/Audios/" . $capitulo . ".mp3";

					$codigoYouTube = $row ['codigoYouTube'];
				}
			}
		}
	}

	if (isset ($datos) and $datos != "")
	{
		return $datos;
	}
}

/**
 * Retorna un array con todas las posibles convinaciones de directorios de autores
 *
 * @param object $db
 *        	Objeto de coneccion a la base de datos.
 * @param int $idAutor
 *        	id del autor a buscar.
 * @throws Exception
 * @return string[]
 */
function getArrayPosNobresAutor($db, $idAutor)
{
	$where = array ();
	$parametros = array ();

	$where [] = " idAutor = :idAutor ";
	$parametros [] = $idAutor;

	if ($where != "")
	{
		$where = implode (" AND ", $where);

		$where = " AND " . $where;
	}

	$sql = "SELECT * FROM Autor WHERE 1=1 " . $where;

	if ($result = $db->query ($sql, $esParam = true, $parametros))
	{
		if ($rst = $db->fetch_array ($result))
		{
			$rst = array_change_key_case ($rst, CASE_LOWER);

			$nombre = str_replace (" ", "_", $rst ['nombre']);
			$apellido = str_replace (" ", "_", $rst ['apellido']);

			$carpeta = array ();

			if (isset ($rst ['segnombre']) and $rst ['segnombre'] != "")
			{
				$segname = str_replace (" ", "_", $rst ['segnombre']);

				$carpeta [] = $apellido . "_" . $nombre . "_" . $segname;
				$carpeta [] = $apellido . "_" . $nombre . "_" . $rst ['segnombre'];
				$carpeta [] = $apellido . "_" . $segname . "_" . $nombre;
				$carpeta [] = $apellido . "_" . $segname . "_" . $rst ['nombre'];
				$carpeta [] = $apellido . "_" . $rst ['nombre'] . "_" . $segname;
				$carpeta [] = $apellido . "_" . $rst ['nombre'] . "_" . $rst ['segnombre'];
				$carpeta [] = $apellido . "_" . $rst ['segnombre'] . "_" . $nombre;
				$carpeta [] = $apellido . "_" . $rst ['segnombre'] . "_" . $rst ['nombre'];
				$carpeta [] = $rst ['apellido'] . "_" . $nombre . "_" . $segname;
				$carpeta [] = $rst ['apellido'] . "_" . $nombre . "_" . $rst ['segnombre'];
				$carpeta [] = $rst ['apellido'] . "_" . $segname . "_" . $nombre;
				$carpeta [] = $rst ['apellido'] . "_" . $segname . "_" . $rst ['nombre'];
				$carpeta [] = $rst ['apellido'] . "_" . $rst ['nombre'] . "_" . $segname;
				$carpeta [] = $rst ['apellido'] . "_" . $rst ['nombre'] . "_" . $rst ['segnombre'];
				$carpeta [] = $rst ['apellido'] . "_" . $rst ['segnombre'] . "_" . $nombre;
				$carpeta [] = $rst ['apellido'] . "_" . $rst ['segnombre'] . "_" . $rst ['nombre'];

				$carpeta [] = $nombre . "_" . $apellido . "_" . $segname;
				$carpeta [] = $nombre . "_" . $apellido . "_" . $rst ['segnombre'];
				$carpeta [] = $nombre . "_" . $segname . "_" . $apellido;
				$carpeta [] = $nombre . "_" . $segname . "_" . $rst ['apellido'];
				$carpeta [] = $nombre . "_" . $rst ['apellido'] . "_" . $segname;
				$carpeta [] = $nombre . "_" . $rst ['apellido'] . "_" . $rst ['segnombre'];
				$carpeta [] = $nombre . "_" . $rst ['segnombre'] . "_" . $apellido;
				$carpeta [] = $nombre . "_" . $rst ['segnombre'] . "_" . $rst ['apellido'];
				$carpeta [] = $rst ['nombre'] . "_" . $apellido . "_" . $segname;
				$carpeta [] = $rst ['nombre'] . "_" . $apellido . "_" . $rst ['segnombre'];
				$carpeta [] = $rst ['nombre'] . "_" . $segname . "_" . $apellido;
				$carpeta [] = $rst ['nombre'] . "_" . $segname . "_" . $rst ['apellido'];
				$carpeta [] = $rst ['nombre'] . "_" . $rst ['apellido'] . "_" . $segname;
				$carpeta [] = $rst ['nombre'] . "_" . $rst ['apellido'] . "_" . $rst ['segnombre'];
				$carpeta [] = $rst ['nombre'] . "_" . $rst ['segnombre'] . "_" . $apellido;
				$carpeta [] = $rst ['nombre'] . "_" . $rst ['segnombre'] . "_" . $rst ['apellido'];

				$carpeta [] = $segname . "_" . $apellido . "_" . $nombre;
				$carpeta [] = $segname . "_" . $apellido . "_" . $rst ['segnombre'];
				$carpeta [] = $segname . "_" . $nombre . "_" . $apellido;
				$carpeta [] = $segname . "_" . $nombre . "_" . $rst ['apellido'];
				$carpeta [] = $segname . "_" . $rst ['apellido'] . "_" . $nombre;
				$carpeta [] = $segname . "_" . $rst ['apellido'] . "_" . $rst ['segnombre'];
				$carpeta [] = $segname . "_" . $rst ['segnombre'] . "_" . $apellido;
				$carpeta [] = $segname . "_" . $rst ['segnombre'] . "_" . $rst ['apellido'];
				$carpeta [] = $rst ['segnombre'] . "_" . $apellido . "_" . $nombre;
				$carpeta [] = $rst ['segnombre'] . "_" . $apellido . "_" . $rst ['nombre'];
				$carpeta [] = $rst ['segnombre'] . "_" . $nombre . "_" . $apellido;
				$carpeta [] = $rst ['segnombre'] . "_" . $nombre . "_" . $rst ['apellido'];
				$carpeta [] = $rst ['segnombre'] . "_" . $rst ['apellido'] . "_" . $nombre;
				$carpeta [] = $rst ['segnombre'] . "_" . $rst ['apellido'] . "_" . $rst ['nombre'];
				$carpeta [] = $rst ['segnombre'] . "_" . $rst ['nombre'] . "_" . $apellido;
				$carpeta [] = $rst ['segnombre'] . "_" . $rst ['nombre'] . "_" . $rst ['apellido'];

				$carpeta [] = $apellido . "_" . $nombre;
				$carpeta [] = $apellido . "_" . $rst ['nombre'];
				$carpeta [] = $rst ['apellido'] . "_" . $nombre;
				$carpeta [] = $rst ['apellido'] . "_" . $rst ['nombre'];

				$carpeta [] = $nombre . "_" . $apellido;
				$carpeta [] = $nombre . "_" . $rst ['apellido'];
				$carpeta [] = $rst ['nombre'] . "_" . $apellido;
				$carpeta [] = $rst ['nombre'] . "_" . $rst ['apellido'];
			}
			else
			{
				$carpeta [] = $apellido . "_" . $nombre;
				$carpeta [] = $apellido . "_" . $rst ['nombre'];
				$carpeta [] = $rst ['apellido'] . "_" . $nombre;
				$carpeta [] = $rst ['apellido'] . "_" . $rst ['nombre'];

				$carpeta [] = $nombre . "_" . $apellido;
				$carpeta [] = $nombre . "_" . $rst ['apellido'];
				$carpeta [] = $rst ['nombre'] . "_" . $apellido;
				$carpeta [] = $rst ['nombre'] . "_" . $rst ['apellido'];
			}

			return $carpeta;
		}
	}
	else
	{
		throw new Exception ('ERROR: No se encontro el autor.');
	}
}

/**
 * Unifica los directorios incluidos en $carpeta usando como maestro el de la primer posicion.
 *
 * @param array $carpeta
 * @return boolean
 */
function corregirEstructuraAutor($carpeta)
{
	$listado = scandir ('Biblioteca');

	foreach ($carpeta as $valor)
	{
		if (isset ($valor) and ($rst ['directorio'] != $valor))
		{
			if (in_array ($valor, $listado))
			{
				copia ('Biblioteca/' . $valor, 'Biblioteca/' . $carpeta [0]);
			}
		}
	}

	return true;
}

/**
 * Recojo el valor de donde copio y donde tengo que copiar.
 * Copia todos los archivos incluidos dentro de $dirOrigen en $dirDestino creando este ultimo en caso de no existir.
 *
 * @param String $dirOrigen
 *        	Directorio de origen
 * @param String $dirDestino
 *        	Directorio de destino
 * @return boolean
 */
function copia($dirOrigen, $dirDestino)
{
	if (! file_exists ($dirDestino))
	{
		// Creo el directorio destino
		mkdir ($dirDestino, 0777, true);
	}

	$carpeta = scandir ($dirOrigen);

	if (count ($carpeta) < 3)
	{
		rmdir ($dirOrigen);
	}
	else
	{
		// abro el directorio origen
		if ($vcarga = opendir ($dirOrigen))
		{
			while ($file = readdir ($vcarga)) // lo recorro enterito
			{
				if ($file != "." && $file != "..") // quito el raiz y el padre
				{
					// echo $file . "!!!";
					// muestro el nombre del archivo
					if (! is_dir ($dirOrigen . $file)) // pregunto si no es directorio
					{
						if (rename ($dirOrigen . "/" . $file, $dirDestino . "/" . $file)) // como no es directorio, copio de origen a destino
						{
							echo " COPIADO!";
						}
						else
						{
							echo " ERROR!";
						}
					}
					else
					{
						echo " — directorio — ";
						// era directorio llamo a la función de nuevo con la nueva ubicación
						copia ($dirOrigen . "/" . $file, $dirDestino . "/" . $file);
					}
					echo "";
				}
			}
			closedir ($vcarga);
		}
	}

	return true;
}

/**
 * Comprueba la existencia del archivo de audio de determinado capitulo.
 *
 * @param object $db
 *        	Objeto de coneccion a la base de datos
 * @param int $idCapitulo
 *        	capitulo con el que trabajar
 * @throws Exception En caso de no poder recuperar el nombre del archivo.
 * @return boolean
 */
function comprobarAudio($db, $idCapitulo)
{
	$param = array ();
	$param ['idCapitulo'] = $idCapitulo;

	$archivo = get_Archivo_capitulo ($db, $param);

	if ($archivo != "")
	{
		if (file_exists ($archivo ['archivoAudio']))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		throw new Exception ('ERROR: No se pudo recuperar el nombre del archivo.');
	}
}
?>