<?php
/*
 * Config mixed by Marcexl
 * version 291225015
 *
 */
if (!isset ($_SESSION))
{
	session_start ();
}

date_default_timezone_set ('America/Buenos_Aires');

$Titulo = 'Portal de Servicios';

$serv = 'DESARROLLO';

// id de la aplicacion
$IDAPLICACION = 31;

// id del modulo
$IDMODULO = 0;

// id del rol que tiene permisos para utilizar esta aplicacion
$IDROL = 214;

/**
 * **********************************************
 * Primero seteamos las bases
 * **********************************************
 */

// acle = oci_connect ($UsuarioOracle, $PasswordOracle, $ServidorOracle, 'WE8ISO8859P1');

$UsuarioOracle = 'internet';
$PasswordOracle = 'internet';
$ServidorOracle = 'PORTAL.DESARROLLO';

$UsuarioOracle2 = 'internet';
$PasswordOracle2 = 'internet';
$ServidorOracle2 = 'ACADEMICA.DESARROLLO';

$UsuarioOracle3 = 'portal';
$PasswordOracle3 = 'portal';
$ServidorOracle3 = 'PADUA3.DESAPOSE';

$CHARACTER_SET = "WE8ISO8859P1";

/**
 * **********************************************
 * Aca van las rutas del la aplicacion
 * **********************************************
 */

$http = "http://serviciosdesa.usal.edu.ar";
$PathCGI = "/cgi-bin/Login/";
$directorio = $http . $PathCGI;
$descargas = 'contador.txt';

$pathGestionJs = $http . "/gestionAcademica/classes/js";
$pathGestionJsConfig = "/config.js";
$pathGestionJsSession = "/session.js";
$pathGestionJsNotificaciones = "/notificaciones.js";
$pathGestionJsLogin = "/controller/login.js";

/**
 * **********************************************
 * Aca van los datos de las credentials en google
 * **********************************************
 */

$client_id = '137063203225-c8omqon8tvl7p85la777hcfqas36jo8m.apps.googleusercontent.com';
$client_secret = 'r2wWYwy1H_sTC9pmTEzqE3Xh';
$redirect_uri = $directorio . 'login.php';

$logoutApp = 'salir.php';
$killGoogle = 'https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=' . $directorio . 'salir.php';

/* */
$JavaScript = "portal.js";
$db_link = "desa_lk";

/* mail */

$emailSet = 'no-reply@usal.edu.ar';
$emailSetUser = 'Atención';
$emailAdress = 'servicios@usal.edu.ar';
$emailAdressUser = 'no-reply';
?>
