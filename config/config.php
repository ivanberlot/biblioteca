<?php
/**
 * Este archivo se encargara de contener e inicializar todas las variables de configuracion que va a utilizar el sistema
 *
 * @author iberlot <@> ivanberlot@gmail.com
 * @version 1.0
 * @package Mytthos
 * @category Config
 *
 */
// header ('Content-Type: text/html;charset=utf-8');
date_default_timezone_set ('America/Argentina/Buenos_Aires');

$dirAutores = "/Biblioteca";

if (!isset ($_SESSION))
{
	session_start ();
}

$host = 'localhost';
$username = 'iberlot';
$password = 'JuliaMatilde';
$database = 'u530625595_libro';

$configured = 1;
$logging = false;
$logfile = 'log.txt';
$allowcommands = TRUE;
$debug = "1";
$debugfile = 'debug.txt';

if (!isset ($sitio))
{
	include_once '/var/www/html/Mytthos/classes/class_sitio.php';

	$sitio = new class_sitio ();
	$sitio->dbSever = $host;
	$sitio->dbUser = $username;
	$sitio->dbPass = $password;
	$sitio->dbBase = $database;
	$sitio->dbCharset = 'utf8';
	$sitio->dbTipo = 'mysql';

	$sitio->dieOnError = $logging;
	$sitio->mostrarErrores = $logging;
	$sitio->debug = $logging; // True si quiero que muestre el Query en por pantalla

	$sitio->nombre = "Mytthos - Biblioteca";
	$sitio->url = "http://pepinos.ddns.net:2550/Mytthos/";
	$sitio->urlCorta = "pepinos.ddns.net:2550/Mytthos/";
	$sitio->emailWebmaster = "ivanberlot@gmail.com";
}

if (!isset ($db))
{
	include_once '/var/www/html/classes/class_db.php';

	$db = new class_db ($sitio->dbSever, $sitio->dbUser, $sitio->dbPass, $sitio->dbBase, $sitio->dbCharset, $sitio->dbTipo, false);
	$db->connect ();

	$db->dieOnError = $sitio->dieOnError;
	$db->mostrarErrores = $sitio->mostrarErrores;
	$db->debug = $sitio->debug;
}

$client_id = '137063203225-c8omqon8tvl7p85la777hcfqas36jo8m.apps.googleusercontent.com';
$client_secret = 'r2wWYwy1H_sTC9pmTEzqE3Xh';

// $redirect_uri = $directorio . 'login.php';
// $redirect_uri = 'http://pepinos.ddns.net:2550/login/login.php';
$redirect_uri = $sitio->url . 'login.php';

?>