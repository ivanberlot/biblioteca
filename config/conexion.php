<?php
/*
 * Archivo que contiene las conexiones a las BD
 * requiere el archivo config.php
 */
 

require_once ("config.php");

/*
 * generamos las conexiones
 */
$linkOracle = oci_connect ($UsuarioOracle, $PasswordOracle, $ServidorOracle, 'WE8ISO8859P1');

$linkOracle2 = oci_connect ($UsuarioOracle, $PasswordOracle, $ServidorOracle2, 'WE8ISO8859P1');

$linkOracle3 = oci_connect ($UsuarioOracle3, $PasswordOracle3, $ServidorOracle3, 'WE8ISO8859P1');

$defconexion = true;

/**
 * Asi mismo le decimos que si no puede conectarse nos comunique el Error
 */

if (! $linkOracle)
{
	$e = oci_error ();
	trigger_error (htmlentities ($e ['message'], ENT_QUOTES), E_USER_ERROR);
}

if (! $linkOracle2)
{
	$e = oci_error ();
	trigger_error (htmlentities ($e ['message'], ENT_QUOTES), E_USER_ERROR);
}
?>

