<?php
/**
 *
 * @author iberlot
 * @version 20151223
 * @package Mytthos
 * @category Config
 *
 *           Este archivo se encargara de contener todos los includes
 *           que va a utilizar el sistema
 *
 */
include_once 'classes/class_sitio.php';
include_once 'classes/class_db.php';
include_once 'classes/class_consultas.php';
include_once 'classes/class_validar.php';
include_once 'classes/class_abm.php';
include_once 'classes/class_orderby.php';
include_once 'classes/class_paginado.php';

include_once ("config/variables.php"); // incluimos el archivo que contiene las variables
                                       // include_once("config/control_session.php");
include_once ("config/config.php"); // incluimos el archivo de configuracion

include_once ("config/conect.php"); // incluimos el archivo de conexion
include_once ("config/funciones.php"); // incluimos el archivo que va a contener todas las funciones del sistem
include_once ("config/styles.css");
include_once ("inc/header.php"); // incluimos el archivo que contiene la cabezera
include_once ("inc/footer.php"); // incluimos el archivo que contiene la cabezera

include_once ("config/analyticstracking.php");
?>

<link rel="stylesheet" href="/classes/font-awesome/css/font-awesome.min.css">
