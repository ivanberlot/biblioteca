<?php
$db = $linkOracle_class->connect ();

date_default_timezone_set ('America/Buenos_Aires');

global $linkOracle;

function mataConect()
{
	if ((isset ($stmt)) and $stmt != "")
	{
		oci_free_statement ($stmt);
	}
	if ((isset ($stmt2)) and $stmt2 != "")
	{
		oci_free_statement ($stmt2);
	}
	if ((isset ($stmt3)) and $stmt3 != "")
	{
		oci_free_statement ($stmt3);
	}
	if ((isset ($stmt4)) and $stmt4 != "")
	{
		oci_free_statement ($stmt4);
	}
	if ((isset ($linkOracle2)) and $linkOracle2 != "")
	{
		oci_close ($linkOracle2);
	}
	if ((isset ($linkOracle)) and $linkOracle != "")
	{
		oci_close ($linkOracle);
	}
}

function convenum($numero)
{ // CONVERTIR NUMEROS
	if ($numero >= 1000)
	{
		$numero = (float) $numero;
		$numero = number_format ($numero, 2, ',', '. ');
	}
	else
	{
		$numero = number_format ($numero, 2, ',', '. ');
	}
	return $numero;
}

function convenum2($numero)
{ // FUNCION INVERSA DE LA ANTERIOR
	$numero = number_format ($numero, 2, '.', ', ');
	return $numero;
}

function compdvario($iddvario, $valor2)
{ // CONVIERTE LOS VALORES DE CODIGOS CORRESPONDIENTE DE LOS DERECHOS VARIOS
                                          // YA QUE LOS CODIGOS EN COBOL CON LOS CODIGOS DE LA TABLA DIFIEREN $valor = "CUOTA SIN VALOR";
                                          // en la tabla se agrego la columna COD_COBOL el numero de "CUOTA SIN VALOR ES 00"
	$valor2 = (int) $valor2;
	
	if ($valor2 == 0)
	{
		
		$query = 'SELECT COD_COBOL FROM CAJA_DERECHOSVARIOS WHERE IDDERECHOSVARIOS= :iddvario';
		
		$param = array (
				':iddvario' => $iddvario 
		);
		
		$statmnt = parametrizar_query ($query, $param);
		
		if ($statmnt)
		{
			$id_mov = oci_fetch_array ($statmnt);
			$iddvario = $id_mov['COD_COBOL'];
		}
	}
	if ($valor2 == 1)
	{
		$query = 'SELECT IDDERECHOSVARIOS FROM CAJA_DERECHOSVARIOS WHERE COD_COBOL= :iddvario';
		
		$param = array (
				':iddvario' => $iddvario 
		);
		
		$statmnt = parametrizar_query ($query, $param);
		
		if ($statmnt)
		{
			$id_mov = oci_fetch_array ($statmnt);
			$iddvario = $id_mov['IDDERECHOSVARIOS'];
		}
	} // ESTA ES LA FUNCION INVERSA
	
	return $iddvario;
}

function pdfcabezera($pdf, $reci)
{ // IMPRIME EL DIBUJO DE RECIBO
	$pdf->AliasNbPages ();
	$pdf->AddPage ();
	$pdf->Image ('../config/imagenes/recibo.png', 0, 0, 0, 0);
	$pdf->SetFont ('Times', '', 13);
	$pdf->ln (-37);
	$pdf->Cell (200, 10, '' . $reci . '', 0, 0, 'C');
	$pdf->ln (37);
	$pdf->SetFont ('Times', '', 8);
	$pdf->Cell (50, 7, '*CUOTA*', 0, 0, 'C');
	$pdf->Cell (9, 7, '*NRO*', 0, 0, 'C');
	$pdf->Cell (20, 7, '*IMPORTE*', 0, 0, 'C');
	$pdf->SetFont ('Times', '', 10);
	return $pdf;
}

// EL CALCULO DE RECARGO DE MATRICULAS EN EL ARCHIVO COBRANZA2.PHP , INDICA LOS RECARGOS , FUERA DE TERMINO DE CADA
// NUMERO DE MATRICULA
function recargomat($i, $fechaven, $pdiario, $IMP_MAT, $c)
{
	global $recdiario;
	global $ftermino;
	
	/* SE TOMA LA FECHA Y EL DIA A LOS QUE PERTENECE LA CAJA ACTUAL */
	$mes = substr ($_SESSION['cod'], -5, 2);
	$hoy = substr ($_SESSION['cod'], -2);
	
	if ($i == $mes)
	{
		if ($hoy <= $fechaven)
		{
			$recargo = 0.00;
		}
		else
		{
			$recargo = $ftermino;
			$recdiario = ((($hoy - $fechaven) * $pdiario) * $IMP_MAT) / 30;
		}
	}
	else
	{
		if ($i > $mes)
		{
			$recargo = $ftermino;
			$recdiario = (((20 + $hoy) * $pdiario) * $IMP_MAT) / 30;
		}
		if ($c < $mes)
		{
			$recargo = 0.00;
			$recdiario = null;
		}
	}
	return array (
			$recargo,
			$recdiario 
	);
}

// CALCULO DE RECARGO DE ARANCELES EN EL ARCHIVO COBRANZA2.PHP , INDICA LOS RECARGOS, FUERA DE TERMINO,LA BONIFICACION
// Y SU MENSAJE
function recargoaran($e, $IMP_ARAN, $fechaven, $pdiario)
{
	global $porcbonif;
	global $msjbonif;
	global $ftermino;
	global $recdiario;
	global $bonif;
	
	/* SE TOMA LA FECHA Y EL DIA A LOS QUE PERTENECE LA CAJA ACTUAL */
	$mes = substr ($_SESSION['cod'], -5, 2);
	$hoy = substr ($_SESSION['cod'], -2);
	
	if ($e < $mes)
	{
		$recargo = $ftermino;
		$recdiario = (((20 + $hoy) * $pdiario) * $IMP_ARAN) / 30;
	}
	else
	{
		if ($e == $mes)
		{
			if ($hoy <= $fechaven)
			{
				$recargo = 0.00;
				if ($hoy <= 5)
				{
					$msjbonif = ' CR';
					$bonif = ($porcbonif * $IMP_ARAN) / 100;
				}
			}
			else
			{
				$msjbonif = null;
				$recargo = $ftermino;
				$recdiario = ((($hoy - $fechaven) * $pdiario) * $IMP_ARAN) / 30;
			}
		}
		else
		{
			if ($e > $mes)
			{
				$recdiario = null;
				$recargo = 0.00;
				$msjbonif = ' CR';
				$bonif = ($porcbonif * $IMP_ARAN) / 100;
			}
			else
			{
				$msjbonif = null;
			}
		}
	}
	return array (
			$recargo,
			$recdiario,
			$msjbonif,
			$bonif 
	);
}

// FUNCION DE NUEVO RECIBO
// AL AGREGAR UN MOVIMIENTO
// SE ENCARGA DE TRAER EL ULTIMO NRO DE RECIBO DE LOS MOVIMIENTOS
// SI NO DISPONE DE UN NRO DE RECIBO
// SE VERIFICA EL NUMERO MAS GRANDE DE RECIBO DE CAJA_REC
// LUEGO SE VERIFICA EL NUMERO MAS GRANDE DE RECIBO DE MOVIMIENTOS
// Y VA SUMANDO CON EL FOR PARA BUSCAR EL NUMERO DISPONIBLE MAS ALTO EN MOVIMIENTOS
// PARA INGRESARLO EN CAJA_REC
// PARA PAGAR SOLO TRAE EL NRO DE RECIBO DIRECTAMENTE
function nuevorecibo($ida, $dni, $linkOracle_class = null)
{
	if ($linkOracle_class == null)
	{
		global $db;
		$linkOracle_class = $db;
	}
	
	// selecciono el ultimo numero que utilizo el usuario y le sumo 1
	$query = 'SELECT  max(NRO)+1 as nro FROM MOVIMIENTOS WHERE PERSON = :ida AND  ESTADO = 2';
	// parametros
	$param = array (
			$ida 
	);
	// ejecuto querie
	$res = $linkOracle_class->query ($query, $esParam = true, $param);
	
	$nro = oci_fetch_array ($res);
	
	$nrorecibo = $nro['NRO'];
	
	// si ese numero da nulo es por que el usuario tiene uno o mas movimientos pendientes , busco los que tengan estado 1
	if ($nrorecibo == NULL)
	{
		// querie que busca los mov activos del alumno
		$query = 'SELECT  max(NRO) as nro FROM MOVIMIENTOS WHERE PERSON = :ida AND  ESTADO = 1';
		// parametros
		$param = array (
				$ida 
		);
		// ejecuto
		$res = $linkOracle_class->query ($query, $esParam = true, $param);
		
		$nro = oci_fetch_array ($res);
		
		$nrorecibo = $nro['NRO'];
		
		// si en esta instancia tdv sigue siendo null el nro de recibo es por que el aluno no tiene ningun movimiento
		// de ningun tipo guardado , en ese caso busco el ultimo numero en la tabla y le sumo 1
		if ($nrorecibo == null)
		{
			$query = 'SELECT  max(NRO) as nro FROM MOVIMIENTOS';
			// parametros
			$param = array (
					$ida 
			);
			// ejecuto
			$res = $linkOracle_class->query ($query, $esParam = true, $param);
			
			$nro = oci_fetch_array ($res);
			
			$nrorecibo = $nro['NRO'];
			// numero recibo si el usuario no tiene nada cargado
			return ($nrorecibo);
		}
		else
		{
			// numero de recibo si el alumno tiene un movimiento o varios sin imprimir
			return $nrorecibo;
		}
	}
	else
	{
		// numero de recibo siguiente si el alumno ya tiene movimientos impresos
		return ($nrorecibo);
	}
}

// OPCION DE CANCELAR RECIBO
// TANTO LOS QUE NO FUERON PAGADOS COMO LOS QUE APARECEN EN DRECIBOS.PHP
// A SU VEZ HABILITADA LA OPCION DE CANCELAR LOS RECIBOS
// PARA ALUMNOS NUEVOS O YA EXISTENTES
// LOS DISCRIMINA CON NUMERO DE DNI, PUEDE QUE HAGA FALTA EL TIPO DE DNI TAMBIEN
function cancelarrecibo($ida, $dni = null, $yapag = null)
{ // aceptar el dni tambien
	global $linkOracle;
	
	$erecibo = "UPDATE TESORERIA.MOVIMIENTOS SET ESTADO=2 WHERE PERSON = $ida AND ESTADO=1";
	
	if ($yapag == null)
	{
		if ($ida == 999999)
		{
			if (isset ($dni))
			{
				$erecibo = $erecibo . " and DNI = $dni";
			}
		}
	}
	else
	{
		$erecibo2 = "UPDATE TESORERIA.CAJA_REC SET ESTADO=2 WHERE PERSON = $ida AND NRO = $yapag";
		$sqerecibo2 = oci_parse ($linkOracle, $erecibo2) or die (' Error en consulta ' . var_dump ($erecibo2) . ' en linea ' . __line__);
		if (oci_execute ($sqerecibo2, OCI_NO_AUTO_COMMIT))
		{
			$r = oci_commit ($linkOracle);
			if (!$r)
			{
				$e = oci_error ($linkOracle);
				trigger_error (htmlentities ($e['message']), E_USER_ERROR);
				oci_rollback ($linkOracle);
				return false;
			}
			else
			{ // SETEO EL ESTADO DEL MOVIMIENTO COMO ANULADO CON EL NRO 3 , Y EN CAJA_REC QUEDA COMO DOS
			         // ESTO TIENE LA FINALIDAD DE DIFERENCIAR CUANDO UN RECIBO ES CANCELADO ANTES DE PAGARLO
			         // Y CUANDO ES ANULADO , PARA LA FINALIDAD DE ASIGNACION DE NUMEROS DE RECIBOS.
				$erecibo = "UPDATE TESORERIA.MOVIMIENTOS SET ESTADO=3 WHERE PERSON = $ida AND ESTADO=0 AND NRO = $yapag";
			}
		}
		else
		{
			oci_rollback ($linkOracle);
			return false;
		}
	}
	
	$sqerecibo = oci_parse ($linkOracle, $erecibo) or die (' Error en consulta ' . var_dump ($erecibo) . ' en linea ' . __line__);
	if (oci_execute ($sqerecibo, OCI_NO_AUTO_COMMIT))
	{
		$r = oci_commit ($linkOracle);
		if (!$r)
		{
			$e = oci_error ($linkOracle);
			trigger_error (htmlentities ($e['message']), E_USER_ERROR);
			oci_rollback ($linkOracle);
			return false;
		}
		else
		{
			return true;
		}
	}
	else
	{
		oci_rollback ($linkOracle);
		return false;
	}
}

// multiples busquedas para cada codigo de opcional dado esto busca por cada codigo
// si dispone de algun importe a cobrar
// extrae el valor retornado de la consulta con la funcion comentada mas abajo
function opcionales($ida, $codigo, $busq, $faesca)
{
	global $linkOracle;
	$anio = date ("Y");
	$anioant = $anio - 1;
	$dni = obtenerdni ($ida);
	$centrocosto = obteneridcentrodecosto ($faesca);
	$aanteriorARAN = "SELECT IMP_ARAN$busq from tesoreria.aux_deudalu where aniocc = $anioant";
	$aactualARAN = "SELECT IMP_ARAN$busq from tesoreria.ccalu where aniocc = $anio";
	$accaluMAT = "SELECT IMP_MAT$busq from TESORERIA.CCALU where aniocc = $anio";
	$aanteriorDEUDAMAT = "SELECT IMP_MAT$busq from tesoreria.aux_deudalu where aniocc = $anioant";
	$concep = $idconcepto = $formatnum = null;
	// echo $codigo;
	switch ($codigo)
	{
		case '5' : // ARANCEL A�O ANT , busco el arancel en deudas del a�o pasado con el dni y la fa que no sea 99
			$concep = "$aanteriorARAN and nrodoc = '$dni' and fa NOT LIKE '99'"; // MODIFICAR ESTO
			break;
		case '02c' : // TRANSPORTE , SELECT userid, MAX(value) KEEP (DENSE_RANK FIRST ORDER BY date DESC) FROM table GROUP BY userid
			$concep = "$aactualARAN and person = '$ida' and fa=99 "; // and fa=99 ?? no hay fa 99 en el idfaesca
			break;
		case '05b' : // TRANSP A�O ANT
			$concep = "$aanteriorARAN and nrodoc = '$dni' and fa=99 ";
			break;
		case '2' : // ARANCEL
		case '02b' : // CURSO DE VERANO
		case '02d' : // PRACTICAS , ES LO MISMO SOLO QUE TIENE LA CONDICION DE LA FAESCA
			if ($codigo != '02d')
			{
				$concep = "$aactualARAN and person = '$ida' and idfaesca=$centrocosto";
			}
			else
			{
				if ($faesca == 030087)
				{
					$concep = "$aactualARAN and person = '$ida' and idfaesca=$centrocosto";
				}
			}
			break;
		case '9' : // MATRI A�O ANT
			$concep = "$aanteriorDEUDAMAT and nrodoc = '$dni' and fa NOT LIKE '99'";
			break;
		case '1' : // MATRICULA
			$concep = "$accaluMAT and person = '$ida' and idfaesca=$centrocosto";
			break;
		case '3' : // TOTAL MATRICULA
			$concep = "SELECT IMP_MAT_UNICA FROM TESORERIA.CCALU where aniocc = $anio and person = '$ida' and idfaesca=$centrocosto";
			break;
		case '4' : // CUOTA PLAN ARANCEL EN CCALU
			$formatnum = 1;
			$concep = "SELECT CUOTA_PLAN_MA$busq FROM TESORERIA.AUX_PPALU where aniopp = $anio and nrodoc_plan = $dni";
			break;
		case '7' : // P PAGO A�O ANT QUE VALORR ??
			$formatnum = 1;
			$concep = "SELECT CUOTA_PLAN$busq FROM TESORERIA.DEPPALU where aniopp = $anioant and nrodoc_plan = $dni";
			break;
		case '8' : // CUOTA PLAN .MATR QUE VALOR ???
			$formatnum = 1;
			$concep = "SELECT CUOTA_PLAN_MA$busq FROM TESORERIA.AUX_PPALU where aniopp = $anio and nrodoc_plan = $dni";
			break;
		case '63' : // MAT NUEVO A�O
			$concep = "SELECT MAT_PROX_AN from TESORERIA.CCALU where aniocc = $anio and nrodoc = '$dni' and idfaesca=$centrocosto";
			break;
		case '32' : // CUOTA ADIC INTER,solo para la carrera 60
			if ($ca == 60)
			{
				$concep = "$aactualARAN and person = $ida and idfaesca=$centrocosto";
			}
			break;
		case '68b' : // PLAN MATR ANT , SOBRE QUE CAMPOS DE MATRICULA ??
			$formatnum = 1;
			$concep = "SELECT CUOTA_PLAN_MA$busq FROM TESORERIA.DEPPALU where aniopp=$anioant and nrodoc_plan = $dni";
			break;
		case '91' : // CURSO DE INGRESO
		case '92' : // CUOTA INGRESO
		case '90' : // MATR A EGRESAR
			$scconcep2 = verificarquerys ("SELECT BECA1 FROM TESORERIA.CCALU where person = $ida and idfaesca=$centrocosto");
			if (oci_execute ($scconcep2))
			{
				while ($arr_asoc = oci_fetch_array ($scconcep2))
				{
					$talumno = $arr_asoc['BECA1'];
				}
			}
			if ((($talumno == 91) or ($talumno == 92)) and ($codigo == 91))
			{
				$concep = "SELECT IMP_ARAN$busq FROM TESORERIA.CCALU where person = $ida and idfaesca=$centrocosto";
			}
			if (($talumno == 13) and ($codigo == 92))
			{
				$concep = "SELECT IMP_ARAN$busq FROM TESORERIA.CCALU where person = $ida and idfaesca=$centrocosto";
			}
			if ((($talumno == 90) or ($talumno == 87)) and ($codigo == 90))
			{
				$concep = "SELECT IMP_ARAN$busq FROM TESORERIA.CCALU where person = $ida and idfaesca=$centrocosto";
			}
			break;
		case '62' : // MUTUO SERIE A
			$concep = null;
			break;
		case '64' : // MUTUO SERIE B
			$concep = null;
			break;
		case '66' : // MUTUO SERIE 1
			$concep = null;
			break;
		case '40' : // MUTUO SERIE C
			$concep = null;
			break;
		case '44' : // DEVOLUC MUTUO
			$concep = null;
			break;
		default :
			break;
	}
	// EXTRAER SEGUNDA PALABRA DE CONCEP PARA OBTENER EL VALOR CORRECTO EN EL ARRAY PARA IDCONCEPTO
	if (isset ($concep))
	{
		$item = explode (' ', trim ($concep));
		if ($item)
		{
			$item2 = $item[1];
			$scconcep = verificarquerys ($concep);
			if ($scconcep)
			{
				oci_execute ($scconcep);
				while ($arr_asoc = oci_fetch_array ($scconcep))
				{
					// QUE ME INDIQUE CUALES SON LAS TABLAS A REVISAR.
					$idconcepto = $arr_asoc[$item2];
				}
				if ($idconcepto <= 0)
				{
					$idconcepto = null;
				}
			}
			else
			{
				echo "Ha sucedido un error al realizar la consulta:<br> $concep<br>";
				$idconcepto = null;
			}
		}
	} // SOLO PARA PLANCUOTAS
	if ($formatnum == 1)
	{
		$idconcepto = substr ($idconcepto, 2);
		$idconcepto = substr ($idconcepto, 0, -2);
	}
	return $idconcepto;
}

// FORMA SIMPLE DE VERIFICACION DE CONSULTAS
// SOLO SE HACE PARA SELECTS , NUNCA PARA INSERTS NI UPDATES EN DICHO CASO SI SE UTILIZA ROLLBACKS Y DEMAS.
function verificarquerys($query)
{
	global $linkOracle;
	$squery = oci_parse ($linkOracle, $query); // or die( ' Error en consulta ' . var_dump( $query ) . ' en linea ' . __line__ )
	if ($squery)
	{
		if (oci_execute ($squery, OCI_NO_AUTO_COMMIT))
		{
			$r = oci_commit ($linkOracle);
			if ($r)
			{
				// echo $query;
				return $squery;
			}
			else
			{
				return null;
			}
			echo "Ha sucedido un error al realizar la consulta:<br> $query<br>";
		}
		else
		{
			echo "Ha sucedido un error al realizar la consulta:<br> $query<br>";
			return null;
		}
	}
	else
	{
		echo "Ha sucedido un error al realizar la consulta:<br> $query<br>";
		return null;
	}
}

// FUNCION DE VERIFICACION SI LA OPCION INGRESADA YA EXISTE PARA EL ALUMNO EN MOVIMIENTOS
// SI ES ASI IMPRIME QUE NO PUEDE SER AGREGADA
// ESTA OPCION SE LLAMA DESDE AJAXMOVIMIENTOS.PHP
function yaenmovimientos($ida, $codigo, $nro, $dni = null)
{
	global $linkOracle;
	
	$nromov = 0;
	// busco en la tabla movimientos si existe algun movimiento en recibo que no se halla liquidado
	$query = "SELECT * FROM TESORERIA.MOVIMIENTOS WHERE PERSON = $ida and CODIGO = '$codigo' and estado = 1";
	
	if ((ctype_digit ($codigo)) and ($nro > 0))
	{ // si es un numero en su totalidad y es mayor a 0
		$smovi = verificarquerys ("$query and pagar = $nro");
	}
	else
	{
		if ($nro == 0)
		{
			$smovi = verificarquerys ("$query");
		}
		else
		{
			$smovi = verificarquerys ("$query and pagar like '%$nro%'");
		}
	}
	
	if ($smovi)
	{
		oci_execute ($smovi);
		$arr_asoc = oci_fetch_array ($smovi);
		$nromov = $arr_asoc['NRO'];
	}
	
	return $nromov;
}

// FUNCION QUE LISTA TODOS LOS RECIBOS PENDIENTES DE SER CANCELADOS ,IMPRESOS,ANULADOS ETC PAGOS..
// SE LLAMA EN SALIR.PHP
function pendientes($caja)
{
	global $linkOracle;
	$i = 0;
	$penden = array ();
	$query = "SELECT PERSON,NRO,DNI,SUM(IMPORTE+RECARGO),AYN,DIRECCION FROM TESORERIA.MOVIMIENTOS WHERE estado = 1 and NROCAJ = $caja GROUP BY PERSON,NRO,DNI,AYN,DIRECCION";
	$squery = verificarquerys ($query);
	if ($squery)
	{
		oci_execute ($squery);
		while ($arr_asoc = oci_fetch_array ($squery))
		{
			$i ++;
			
			$PENDEN[$i][0] = $arr_asoc['PERSON'];
			$PENDEN[$i][1] = $arr_asoc['SUM(IMPORTE+RECARGO)'];
			$PENDEN[$i][2] = $arr_asoc['NRO'];
			$PENDEN[$i][3] = $arr_asoc['DNI'];
			$PENDEN[$i][4] = $arr_asoc['AYN'];
			$PENDEN[$i][5] = $arr_asoc['DIRECCION'];
		}
	}
	if (isset ($PENDEN))
	{
		return $PENDEN;
	}
	else
	{
		return null;
	}
}

// LISTA EL DNI CON EL PERSON
function obtenerdni($ida)
{
	global $linkOracle;
	$scdni = verificarquerys ("SELECT DOCNO FROM APPGRAL.PERDOC WHERE PERSON = $ida");
	if ($scdni)
	{
		oci_execute ($scdni);
		$arr_asoc = oci_fetch_array ($scdni);
		$dni = $arr_asoc['DOCNO'];
	}
	else
	{
		$dni = null;
	}
	return $dni;
}

// LISTA LA IDFAESCA CON EL PERSON
function obtenerfaesca($ida)
{
	global $linkOracle;
	$scfaes = verificarquerys ("SELECT IDFAESCA FROM TESORERIA.MOVIMIENTOS WHERE PERSON = $ida");
	if ($scfaes)
	{
		oci_execute ($scfaes);
		$arr_asoc = oci_fetch_array ($scfaes);
		$faes = $arr_asoc['IDFAESCA'];
	}
	else
	{
		$faes = null;
	}
	return $faes;
}

// LISTA EL NOMBRE Y APELLIDO CON EL DNI
function obtenerayn($dni)
{
	global $linkOracle;
	$scayn = verificarquerys ("SELECT AYN FROM INTERFAZ.AUX_CCALU WHERE NRODOC = '$dni'");
	if ($scayn)
	{
		oci_execute ($scayn);
		$arr_asoc = oci_fetch_array ($scayn);
		$ayn = $arr_asoc['AYN'];
	}
	else
	{
		$ayn = null;
	}
	return $ayn;
}

// SETEA SI LA IMPRESION DEL RECIBO FUE CORRECTA
function setearimpresion($ida = 999999, $dni = null)
{ // aceptar el dni tambien
	global $linkOracle;
	$erecibo = "UPDATE TESORERIA.MOVIMIENTOS SET IMPRIMIO=1 WHERE PERSON = $ida AND ESTADO=1";
	if ($ida == 999999)
	{
		$erecibo = $erecibo . " and DNI = $dni";
	}
	$sqerecibo = oci_parse ($linkOracle, $erecibo) or die (' Error en consulta ' . var_dump ($erecibo) . ' en linea ' . __line__);
	if (oci_execute ($sqerecibo, OCI_NO_AUTO_COMMIT))
	{
		$r = oci_commit ($linkOracle);
		if (!$r)
		{
			$e = oci_error ($linkOracle);
			trigger_error (htmlentities ($e['message']), E_USER_ERROR);
			return false;
		}
		else
		{
			return true;
		}
	}
	else
	{
		return false;
	}
}

// CHEQUEA LA IMPRESION FUE CORRECTA O NO,
// ESTA FUNCION SE UTILIZA CUANDO SE REALIZA EL PAGO Y A VECES NO SE HACE EL RELOAD
// PARA QUE HABILITE LA OPCION PAGAR, ENTONCES SE PREVIENE QUE SE CANCELE O SE PAGUE NUEVAMENTE
// SI YA FUE IMPRESO(SI ES ASI YA FUE PAGA.)
function chequearimpresion($ida, $dni = null)
{ // aceptar el dni tambien
	global $linkOracle;
	$erecibo = "SELECT count(imprimio) FROM TESORERIA.MOVIMIENTOS WHERE PERSON = $ida AND ESTADO=1";
	
	$sqerecibo = oci_parse ($linkOracle, $erecibo) or die (' Error en consulta ' . var_dump ($erecibo) . ' en linea ' . __line__);
	if (oci_execute ($sqerecibo, OCI_NO_AUTO_COMMIT))
	{
		$r = oci_commit ($linkOracle);
		if (!$r)
		{
			$e = oci_error ($linkOracle);
			trigger_error (htmlentities ($e['message']), E_USER_ERROR);
			return false;
		}
		else
		{
			$arr_asoc = oci_fetch_array ($sqerecibo);
			if ($arr_asoc['COUNT(IMPRIMIO)'] >= 1)
			{
				return true;
			}
			else
			{
				return null;
			}
		}
	}
	else
	{
		return null;
	}
}

// FUNCION QUE VERIFICA LOS DIAS LABORALES A PARTIR DE DOS FECHAS
function dlaborales($desde, $hasta)
{
	$desde_array = explode ("/", $desde);
	$hasta_array = explode ("/", $hasta);
	$dsemana = "$desde_array[2]/$desde_array[1]/$desde_array[0]";
	$dsemana = date ('w', strtotime ($dsemana));
	$dsemana2 = "$hasta_array[2]/$hasta_array[1]/$hasta_array[0]";
	$dsemana2 = date ('w', strtotime ($dsemana2));
	$dlab = null;
	
	if (($dsemana == 6) or ($dsemana == 0))
	{
		$dlab = 1;
	}
	if (($dsemana2 == 6) or ($dsemana2 == 0))
	{
		$dlab = 2;
	}
	return $dlab;
}

// FUNCIONES PARA COMPARAR FECHAS EN ECAJA.PHP
function sabermes($desde, $hasta)
{
	$retval = "";
	
	$exdesde = explode ('/', $desde);
	$exhasta = explode ('/', $hasta);
	
	if (is_array ($exdesde) && is_array ($exhasta))
	{
		$difanio = $exhasta[2] - $exdesde[2];
		$difMes = $exhasta[1] - $exdesde[1];
		$difDias = $exhasta[0] - $exdesde[0];
		$retval = $difMes + 1;
		$retval += $difanio * 12;
	}
	return array (
			$retval,
			$exdesde[1],
			$exdesde[2],
			$difanio 
	);
}

// OPCION PARA IMPRIMIR GRAFICO , SI MAL NO RECUERDO TRAE TODOS LOS IMPORTES DE CADA DIA DE LA SEMANA PASADA
function sgrafico()
{
	global $linkOracle;
	$semanapago = array ();
	$semanadias = array ();
	$prinsemana = date ('Y/m/d', strtotime (date ("Y") . "W" . date ("W"))) . "\n";
	$prinsemana2 = preg_replace ("/(\d+)\D+(\d+)\D+(\d+)/", "$3-$2-$1", $prinsemana);
	$dateArr = explode ('-', $prinsemana2);
	$stmt = verificarquerys ("SELECT * FROM TESORERIA.CAJA_REC WHERE to_char(FECHA,'dd/mm/yyyy') LIKE '$dateArr[0]/$dateArr[1]/%'");
	for($i = 0; $i <= 4; $i ++)
	{
		$semanapago[$i] = 0;
		$semanadias[$i] = $dateArr[0];
		if (isset ($stmt))
		{
			if (oci_execute ($stmt))
			{
				while ($arr_asoc15 = oci_fetch_array ($stmt))
				{
					$semanapago[$i] += $arr_asoc15['IMPOR_FONDO'];
				}
			}
		}
		
		$prinsemana2 = date ("Y/m/d", strtotime ($prinsemana2 . "+1 days"));
		$prinsemana2 = preg_replace ("/(\d+)\D+(\d+)\D+(\d+)/", "$3-$2-$1", $prinsemana2);
		$dateArr = explode ('-', $prinsemana2);
		$stmt = verificarquerys ("SELECT * FROM TESORERIA.CAJA_REC WHERE to_char(FECHA,'dd/mm/yyyy') LIKE '$dateArr[0]/$dateArr[1]/%'");
	}
	return array (
			$semanadias,
			$semanapago 
	);
}

// TRAE TODOS LOS IMPORTES DE LOS RECIBOS EN MOVIMIENTOS YA SEAN
// CANCELADOS = ESTADO = 2
// PAGOS = ESTADO = 0
// PENDIENTES = ESTADO = 1
// ANULADO SERIA 3 (EN CAJA_REC)), FALTARIA AGREGAR ESTA OPCION
function dpagos()
{
	global $linkOracle;
	global $fecha;
	$diapagos = array (
			0,
			0,
			0 
	);
	$stmt = verificarquerys ("SELECT * FROM TESORERIA.MOVIMIENTOS WHERE to_char(FECHA_TRANS,'dd/mm/yyyy') = '$fecha'");
	if (isset ($stmt))
	{
		if (oci_execute ($stmt))
		{
			while ($arr_asoc14 = oci_fetch_array ($stmt))
			{
				if ($arr_asoc14['ESTADO'] == 0)
				{
					$diapagos[0] += $arr_asoc14['IMPORTE'];
				}
				if ($arr_asoc14['ESTADO'] == 1)
				{
					$diapagos[1] += $arr_asoc14['IMPORTE'];
				}
				if ($arr_asoc14['ESTADO'] == 2)
				{
					$diapagos[2] += $arr_asoc14['IMPORTE'];
				}
			}
		}
		else
		{
			$diapagos = null;
		}
	}
	else
	{
		$diapagos = null;
	}
	return $diapagos;
}

// ESTADISTICAS DEL A�O ACTUAL Y EL A�O ANTERIOR Y EL A�O ANTERIOR LOS 6 MESES
// ESTA FUNCION SIRVE PARA COMPARAR DOS A�OS ANTERIORES EN ECAJA.PHP
// Y A LA VEZ TRAER LOS 6 MESES ANTERIORES DE CADA A�O
// POR EJ ESTAMOS EN DICIEMBRE DEL 2016
// TRAEMOS
// SEPTIEMBRE AGOSTO SEPTIEMBRE OCTUBRE NOVIEMBRE DICIEMBRE 2016
// SEPTIEMBRE AGOSTO SEPTIEMBRE OCTUBRE NOVIEMBRE DICIEMBRE 2015
// Y TRAEMOS CADA IMPORTE DE CADA MES Y DEPENDIENDO EL A�O HACEMOS UNA CONSULTA
// EN EL HISTORIAL O NO
function ultseismeses()
{
	global $mes;
	global $anio;
	global $linkOracle;
	$fechaaltasistema = 2016; // IMPORTANTE!!!!!! MODIFICAR CON FECHA DE INICIO DEL USO DEL SISTEMA
	$mesesesp = array (
			"Enero",
			"Febrero",
			"Marzo",
			"Abril",
			"Mayo",
			"Junio",
			"Julio",
			"Agosto",
			"Setiembre",
			"Octubre",
			"Noviembre",
			"Diciembre" 
	);
	if ($mes > 6)
	{
		$cmes = $mes - 6;
	}
	else
	{
		$cmes = $mes;
	}
	$ultmes = 12 - $cmes;
	$mespagar = array ();
	$mespagar2 = array ();
	$mespagar3 = array ();
	$anioant = $anio - 1;
	$anioantt = $anioant - 1;
	
	if ($cmes >= 1)
	{
		for($i = 1; $i <= 6; $i ++)
		{
			
			$meses = $mesesesp[$cmes];
			$mespagar[0][$i] = '"' . $meses . '"';
			
			$mespagar[1][$i] = 0;
			$mespagar2[1][$i] = 0;
			$mespagar2[0][$i] = '"' . $meses . '"';
			$mespagar3[1][$i] = 0;
			$mespagar3[0][$i] = $meses;
			$stmt = verificarquerys ("SELECT * FROM TESORERIA.CAJA_REC WHERE to_char(FECHA,'dd/mm/yyyy') LIKE '%/$cmes/$anio'");
			
			if (isset ($stmt))
			{
				if (oci_execute ($stmt))
				{
					while ($arr_asoc12 = oci_fetch_array ($stmt))
					{
						if ($arr_asoc12['ESTADO'] == 0)
						{
							$mespagar[1][$i] += $arr_asoc12['IMPOR_FONDO'];
						}
					}
				}
			}
			
			$stmt2 = verificarquerys ("select * from interfaz.recaudacion where FECHA LIKE '$anioant%$cmes%'");
			if (isset ($stmt2))
			{
				if (oci_execute ($stmt2))
				{
					while ($arr_asoc13 = oci_fetch_array ($stmt2))
					{
						$mespagar2[1][$i] += $arr_asoc13['IMPORTE'];
					}
				}
			}
			
			$stmt3 = verificarquerys ("select * from interfaz.recaudacion where FECHA LIKE '$anioantt%$cmes%'");
			if (isset ($stmt3))
			{
				if (oci_execute ($stmt3))
				{
					while ($arr_asoc14 = oci_fetch_array ($stmt3))
					{
						$mespagar3[1][$i] += $arr_asoc14['IMPORTE'];
					}
				}
			}
			$cmes ++;
		}
	}
	else
	{
		// INDICA QUE SE PASO HASTA EL OTRO A�O
		
		for($i = 1; $i <= 6; $i ++)
		{
			
			// $meses2 = date("F", mktime(0, 0, 0, $ultmes, 10));
			$meses = $mesesesp[$ultmes];
			$mespagar[0][$i] = '"' . $meses . '"';
			$mespagar[1][$i] = 0;
			$mespagar2[0][$i] = '"' . $meses . '"';
			$mespagar2[1][$i] = 0;
			$mespagar3[1][$i] = 0;
			$mespagar3[0][$i] = $cmes;
			if ($ultmes > 12)
			{
				$ultmes = 1;
				$anioant ++; // PASO
				$anioantt ++;
			}
			else
			{
				$ultmes ++; // }
			}
			
			if ($anioant < $fechaaltasistema)
			{
				$stmt2 = verificarquerys ("select * from interfaz.recaudacion where FECHA LIKE '$anioant%$ultmes%'");
				if (isset ($stmt2))
				{
					if (oci_execute ($stmt2))
					{
						while ($arr_asoc13 = oci_fetch_array ($stmt2))
						{
							
							$mespagar2[1][$i] += $arr_asoc13['IMPORTE'];
						}
					}
				}
				
				$stmt3 = verificarquerys ("select * from interfaz.recaudacion where FECHA LIKE '$anioantt%$cmes%'");
				if (isset ($stmt3))
				{
					if (oci_execute ($stmt3))
					{
						while ($arr_asoc14 = oci_fetch_array ($stmt3))
						{
							$mespagar3[1][$i] += $arr_asoc14['IMPORTE'];
						}
					}
				}
			}
			else
			{
				$stmt = verificarquerys ("SELECT * FROM TESORERIA.CAJA_REC WHERE to_char(FECHA,'dd/mm/yyyy') LIKE '%/$ultmes/$anioant'");
				// echo "SELECT * FROM TESORERIA.CAJA_REC WHERE to_char(FECHA,'dd/mm/yyyy') LIKE '%/$ultmes/$anioant'";
				if (isset ($stmt))
				{
					if (oci_execute ($stmt))
					{
						while ($arr_asoc12 = oci_fetch_array ($stmt))
						{
							if ($arr_asoc12['ESTADO'] == 0)
							{
								$mespagar[1][$i] += $arr_asoc12['IMPOR_FONDO'];
							}
						}
					}
				}
			}
		}
	}
	return array (
			$mespagar,
			$mespagar2,
			$mespagar3 
	);
}

// ESTADISTICAS DEL MES DEL A�O ACTUAL COMPARADO CON EL MES DEL A�O ANTERIOR
// TRAE LOS IMPORTES DE CADA MES.
function ultmes()
{
	global $mes;
	global $anio;
	global $linkOracle;
	$mesactual = $mesant = null;
	$anioant = $anio - 1;
	$stmt = verificarquerys ("SELECT * FROM TESORERIA.CAJA_REC WHERE to_char(FECHA,'dd/mm/yyyy') LIKE '%/$mes/$anio'");
	$stmt2 = verificarquerys ("select * from interfaz.recaudacion where FECHA LIKE '$anioant%$mes%'");
	if (isset ($stmt))
	{
		if (oci_execute ($stmt))
		{
			while ($arr_asoc1 = oci_fetch_array ($stmt))
			{
				if ($arr_asoc1['ESTADO'] == 0)
				{
					$mesactual += $arr_asoc1['IMPOR_FONDO'];
				}
			}
		}
	}
	
	if (isset ($stmt2))
	{
		if (oci_execute ($stmt2))
		{
			while ($arr_asoc2 = oci_fetch_array ($stmt2))
			{
				
				$mesant += $arr_asoc2['IMPORTE'];
			}
		}
	}
	
	return array (
			$mesant,
			$mesactual 
	);
}

// TRAE LOS ROLES DE LA APLICACION
function permisos()
{
	global $linkOracle2;
	global $IDAPLICACION;
	$roles = $usuarios = array ();
	$i = $n = 0;
	$roles[$i][$n] = 0;
	// $roles[$i][0][0]=$arr_asoc['IDMODULO'];
	// $idmodulo = $arr_asoc['IDMODULO']; //$IDAPLICACION
	$consul2 = "select DISTINCT IDROL FROM appadmusu.moduloxrol where IDAPLICACION = $IDAPLICACION AND ACTIVO = 1";
	$stmt2 = oci_parse ($linkOracle2, $consul2);
	if ($stmt2)
	{
		if (oci_execute ($stmt2))
		{
			while ($arr_asoc2 = oci_fetch_array ($stmt2))
			{
				$idrol = $arr_asoc2['IDROL'];
				$consul4 = "select DESCRIPCION FROM appadmusu.rol where IDROL = $idrol AND ACTIVO = 1";
				
				$stmt4 = oci_parse ($linkOracle2, $consul4);
				if ($stmt4)
				{
					if (oci_execute ($stmt4))
					{
						while ($arr_asoc4 = oci_fetch_array ($stmt4))
						{
							$roles[$i][$n] = $arr_asoc4['DESCRIPCION'] . '+' . $idrol . '';
							$n ++;
						}
					}
					else
					{
						return null;
					}
				}
				else
				{
					return null;
				}
			}
		}
		else
		{
			return null;
		}
	}
	else
	{
		return null;
	}
	
	// $i++;
	return $roles;
}

// trae las cajas validas habilitadas para el person loggeado
function cajasvalidas($person)
{
	global $linkOracle;
	// if (isset($_SESSION['person']))
	// {
	// $cajera = $_SESSION['person'];
	// }
	
	$cajas = null;
	$stmt = verificarquerys ("SELECT IDCAJA FROM TESORERIA.USUARIOCAJA WHERE PERSON = $person ORDER BY IDCAJA");
	if (isset ($stmt))
	{
		if (oci_execute ($stmt))
		{
			
			while ($arr_asoc = oci_fetch_array ($stmt))
			{
				$cajas .= '<option value="' . $arr_asoc['IDCAJA'] . '">' . $arr_asoc['IDCAJA'] . '</option>';
			}
			
			return $cajas;
		}
		else
		{
			return null;
		}
	}
	else
	{
		return null;
	}
}

// trae el idcentrodecostos
function obteneridcentrodecosto($faesca)
{
	global $linkOracle;
	$idcentrodecosto = null;
	$query2 = "select idcentrodecosto from contaduria.centrodecosto where" . " substr('000000'||faesca,-6) = substr('000000'||:faesca,-6)";
	
	// $query2_debug="select idcentrodecosto from contaduria.centrodecosto where substr('000000'||faesca,-6) = substr('000000'||$faesca,-6)";
	
	$stmt2 = oci_parse ($linkOracle, $query2);
	oci_bind_by_name ($stmt2, ":faesca", $faesca);
	
	// echo $query2_debug;
	
	if (isset ($stmt2))
	{
		if (oci_execute ($stmt2))
		{
			while ($arr_asoc2 = oci_fetch_array ($stmt2))
			{
				$idcentrodecosto = $arr_asoc2['IDCENTRODECOSTO'];
			}
		}
	}
	return $idcentrodecosto;
}

// FUNCION PARA EL RECARGO DE MUTUOS
function recargomutuos($indice)
{
	global $linkOracle;
	
	// obtener el indice actual
	$aahoy = date ("Y");
	$mmhoy = date ("m");
	$ddhoy = date ("d");
	$consindice = verificarquerys ("SELECT * FROM TESORERIA.INDICEMUT 
	          WHERE TIPO_INDICE = $indice and
	                ANIO <= $aahoy and
	    	        MES  <= $mmhoy and
	    		    DIA  <= $ddhoy");
	
	switch ($indice)
	{
		case 'D' :
		case 'M' :
		case ' ' :
		/*
		 * * -Leer la tabla INDICEMUT y buscar el TIPO DE INDICE y con fecha h�bil anterior a la fecha del sistema, el valor del �ndice (INDICE)
		 * - Leer la tabla MUTUOS2 , (figuran los datos del contrato) para obtener la fecha de alta del mutuo FECHA-ALTA. Con la fecha de alta , buscar el valor del �ndice , en la tabla INDICEMUT
		 * Calcular el INDICE = valor del �ndice de la fecha actual / valor del �ndice de la fecha alta del mutuo.
		 * Calcular el recargo como : (Valor de la cuota * el INDICE ) - valor de la cuota*
		 */
		default :
			// CONSULTAR INDICE DE LA TABLA DE RECARGOS, RECA_INDICE
			break;
	}
}

function redireccionar_usuario($op)
{
	switch ($op)
	{
		case '2' :
			header ('location: dvarios.php');
			break;
		case '3' :
			header ('location: opcionales.php?ida=' . $ida . '');
			break;
		case '4' :
			header ('location: dprestamos.php');
			break;
		case '5' :
			header ('location: canteriores.php');
			break;
		case '6' :
			header ('location: recibos.php');
			break;
		case '7' :
			header ('location: drecibos.php');
			break;
		case '8' :
			header ('location: acursnivel.php');
			break;
		case '9' :
			header ('location: cmutuos.php');
			break;
		case '10' :
			$webope = '<fieldset>Total deuda';
			break;
		case '11' :
			header ('location: ccaja.php');
			break;
		default :
			header ('location: cobranza.php');
			break;
	}
}

function parametrizar_query($query, $array_parametros)
{
	global $linkOracle;
	
	$stmt = oci_parse ($linkOracle, $query) or die (' Error en consulta ' . var_dump ($query) . ' en linea ' . __line__);
	
	/* Recorro el array y reemplazo los parametros */
	foreach ($array_parametros as $key => $value)
	{
		oci_bind_by_name ($stmt, $key, $array_parametros[$key]) or die (' Error en consulta function parametrizar queries queries = ' . var_dump ($query) . '----' . var_dump ($key) . ' en linea ' . __line__);
	}
	
	/* Ejecuto el statment */
	oci_execute ($stmt, OCI_COMMIT_ON_SUCCESS);
	
	return $stmt;
}

function ya_regis_dia($id_alumno, $codigo_mov_pagar, $nro_mov, $fecha)
{
	global $linkOracle;
	
	// parametros a pasar
	$param = array (
			':person' => $id_alumno,
			':codigo' => $codigo_mov_pagar,
			':nro_mov' => $nro_mov,
			':fecha' => $fecha 
	);
	// parametrizo querie
	$query = 'select * from movimientos where PERSON = :person  ' . 'AND CODIGO = :codigo ' . 'AND NRO_MOV = :nro_mov  AND ESTADO = 1' . 'AND to_char(FECHA_TRANS,"dd/mm/yy") = :fecha';
	
	$query_debug = "select * from movimientos where PERSON = $id_alumno  
    AND CODIGO = $codigo_mov_pagar AND NRO_MOV = $nro_mov AND estado = 1 and  to_char(FECHA_TRANS,'dd/mm/yy') = '$fecha' ";
	
	$stmt = oci_parse ($linkOracle, $query_debug) or die (' Error en consulta ' . var_dump ($query) . ' en linea ' . __line__);
	
	/* Ejecuto el statment */
	oci_execute ($stmt, OCI_NO_AUTO_COMMIT);
	
	/* $stmt = parametrizar_query($query ,$param); */
	$person = oci_fetch_array ($stmt);
	
	if ($person[0]['IDMOVIMIENTO'])
	{
		return $person[0]['IDMOVIMIENTO'];
	}
	else
	{
		return false;
	}
}

function conocer_ultimo_mov()
{
	$query = "select MAX(IDMOVIMIENTO) AS ID FROM MOVIMIENTOS  ";
	
	global $linkOracle;
	
	$stmt = oci_parse ($linkOracle, $query);
	
	oci_execute ($stmt, OCI_NO_AUTO_COMMIT);
	
	if ($stmt)
	{
		$arr_asoc = oci_fetch_array ($stmt);
		return $arr_asoc['ID'];
	}
}

function conocer_ultimo_nro_ID($tabla, $campo)
{
	$query = "select MAX($campo) AS ID FROM $tabla  ";
	
	global $linkOracle;
	
	$stmt = oci_parse ($linkOracle, $query);
	
	oci_execute ($stmt, OCI_NO_AUTO_COMMIT);
	
	if ($stmt)
	{
		$arr_asoc = oci_fetch_array ($stmt);
		return $arr_asoc['ID'];
	}
}

function conocer_ultimo_nro_recibo()
{
	$query = "select MAX(NRO_RECIBO) AS ID FROM RECIBOS_CAJA  ";
	
	global $linkOracle;
	
	$stmt = oci_parse ($linkOracle, $query);
	
	oci_execute ($stmt, OCI_NO_AUTO_COMMIT);
	
	if ($stmt)
	{
		$arr_asoc = oci_fetch_array ($stmt);
		return $arr_asoc['ID'];
	}
}
?>




