DIRDESDE=$1
DIRHASTA=$2
echo "Desde: "
echo  ${DIRDESDE}
echo "Hasta: "
echo  ${DIRHASTA}

mv ${DIRDESDE}/Capitulos/* ${DIRHASTA}/Capitulos/
mv ${DIRDESDE}/Audios/* ${DIRHASTA}/Audios/
mv ${DIRDESDE}/Archivos/* ${DIRHASTA}/Archivos/
rm ${DIRDESDE}/Archivos -R
rm ${DIRDESDE}/Capitulos -R
rm ${DIRDESDE}/Audios -R
mv ${DIRDESDE}/* ${DIRHASTA}/
ls -la ${DIRDESDE}/
rm ${DIRDESDE} -R


