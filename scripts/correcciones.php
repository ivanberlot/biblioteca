<?php

/**
 *
 * @author iberlot <@> ivanberlot@gmail.com
 * @todo FechaC 23/10/2017 - Lenguaje PHP
 *
 * @name tablasue_cpy.php
 *
 * @todo Manejo de los datos de tablasue.
 *
 * @version 0.1 - Version de inicio
 *
 * @package Sueldos
 *
 * @category pruebasPHP
 *
 * @link ../config/includes.php - Archivo con todos los includes del sistema
 */
/*
 * Querido programador:
 *
 * Cuando escribi este codigo, solo Dios y yo sabiamos como funcionaba.
 * Ahora, Solo Dios lo sabe!!!
 *
 * Asi que, si esta tratando de 'optimizar' esta rutina y fracasa (seguramente),
 * por favor, incremente el siguiente contador como una advertencia para el
 * siguiente colega:
 *
 * totalHorasPerdidasAqui = 5
 *
 */
header ('Content-Type: text/html; charset=UTF-8');

require_once ("config/includes.php");

require_once ("classes/class_db.php");

$db = new class_db ($host, $username, $password, $database, '', 'mysql');
$db->connect ();

// $db->dieOnError = $dbDieOnError;
// $db->mostrarErrores = $dbMostrarErrores;
// $db->debug = $dbDebug; // True si quiero que muestre el Query en por pantalla
$db->dieOnError = true;
$db->mostrarErrores = true;
$db->debug = true; // True si quiero que muestre el Query en por pantalla

limpiar_acentos ($db, 'Capitulo', 'foto');
limpiar_acentos ($db, 'Libro', 'titulo');

$sql = "SELECT Capitulo.idCapitulo, Capitulo.nrOrden, Capitulo.titulo, Capitulo.foto, Libro.titulo Libro, Libro.ordenSaga FROM Capitulo INNER JOIN Libro ON Capitulo.idLibro = Libro.idLibro";

$result = $db->query ($sql);

while ($fetch = $db->fetch_assoc ($result))
{
	$parametros = array ();

	$archivo = str_pad ($fetch['ordenSaga'], 2, "0", STR_PAD_LEFT) . "-" . $fetch['Libro'] . "-Capitulo_" . str_pad ($fetch['nrOrden'], 2, "0", STR_PAD_LEFT) . "-" . $fetch['titulo'];

	$archivo2 = $fetch['foto'];

	echo $archivo;

	echo "   ===>   ";

	$archivoNuevo = str_replace (" ", "_", $archivo);

	$archivoNuevo = ucwords (strtolower ($archivoNuevo), "_-");

	echo $archivoNuevo;

	echo "<br>";
	if ($archivo2 != $archivoNuevo)
	{
		if ($archivoNuevo != $archivo)
		{
			$sqlUpdate = "UPDATE Capitulo SET foto = '$archivoNuevo' WHERE idCapitulo = " . $fetch['idCapitulo'];

			$db->query ($sqlUpdate);
		}
	}
}

$fichero = scandir ("Biblioteca/");
echo "<br>";
echo "<br>";
echo "<br>";
echo "<br>";
echo "<br>";
print_r ($fichero);
echo "<br>";
echo "<br>";
echo "<br>";
echo "<br>";
foreach ($fichero as &$autor)
{

	if ((is_dir ("Biblioteca/" . $autor)) and $autor != "." and $autor != "..")
	{
		// renombrar_capitalizado ("Biblioteca/", $autor);

		$sagas = scandir ("Biblioteca/" . $autor);

		foreach ($sagas as &$saga)
		{
			$baseS = "Biblioteca/" . $autor;

			// renombrar_capitalizado ($baseS, $autor);

			if ((is_dir ($baseS . "/" . $saga)) and $saga != "." and $saga != "..")
			{

				// crear_estructura ($baseS);

				$baseL = "Biblioteca/" . $autor . "/" . $saga;

				// renombrar_capitalizado ($baseL, $autor);

				crear_estructura ($baseL);

				$libros = scandir ($baseL);

				foreach ($libros as &$libro)
				{

					if ((is_dir ("Biblioteca/" . $autor . "/" . $saga . "/" . $libro)) and $libro != "." and $libro != "..")
					{
						crear_estructura ("Biblioteca/" . $autor . "/" . $saga . "/" . $libro);

						$capitulos = scandir ("Biblioteca/" . $autor . "/" . $saga . "/" . $libro);

						foreach ($capitulos as &$capitulo)
						{
							$baseC = "Biblioteca/" . $autor . "/" . $saga . "/" . $libro . "/";

							if ((!is_dir ($baseC . $capitulo)) and $capitulo != "." and $capitulo != "..")
							{
								renombrar_capitalizado ($baseC, $capitulo);
							}
							else
							{

								crear_estructura ($baseC);
							}
						}
					}
				}
			}
		}
	}
}

function crear_estructura($dirBase)
{
	if (!file_exists ($dirBase . "/Archivos"))
	{
		mkdir ($dirBase . "/Archivos");
	}

	if (!file_exists ($dirBase . "/Audios"))
	{
		mkdir ($dirBase . "/Audios");
	}

	if (!file_exists ($dirBase . "/Capitulos"))
	{
		mkdir ($dirBase . "/Capitulos");
	}
}

function renombrar_capitalizado($base, $viejo, $mostrar = false)
{
	$nuevo = ucwords (strtolower ($viejo), "_-");

	if ($mostrar == true)
	{
		echo $viejo;

		echo "   ===>   ";

		echo $nuevo;

		echo "<br>";
	}

	rename ($base . $viejo, $base . $nuevo);
}

function limpiar_acentos($db, $tabla, $campo)
{
	$sql = "UPDATE " . $tabla . " SET " . $campo . " = REPLACE(" . $campo . ",'�', 'e')";
	$db->query ($sql);
	$sql = "UPDATE " . $tabla . " SET " . $campo . " = REPLACE(" . $campo . ",'�', 'i')";
	$db->query ($sql);
	$sql = "UPDATE " . $tabla . " SET " . $campo . " = REPLACE(" . $campo . ",'�', 'o')";
	$db->query ($sql);
	$sql = "UPDATE " . $tabla . " SET " . $campo . " = REPLACE(" . $campo . ",'�', 'u')";
	$db->query ($sql);
	$sql = "UPDATE " . $tabla . " SET " . $campo . " = REPLACE(" . $campo . ",'�', 'A')";
	$db->query ($sql);
	$sql = "UPDATE " . $tabla . " SET " . $campo . " = REPLACE(" . $campo . ",'�', 'E')";
	$db->query ($sql);
	$sql = "UPDATE " . $tabla . " SET " . $campo . " = REPLACE(" . $campo . ",'�', 'I')";
	$db->query ($sql);
	$sql = "UPDATE " . $tabla . " SET " . $campo . " = REPLACE(" . $campo . ",'�', 'O')";
	$db->query ($sql);
	$sql = "UPDATE " . $tabla . " SET " . $campo . " = REPLACE(" . $campo . ",'�', 'U')";
	$db->query ($sql);
	$sql = "UPDATE " . $tabla . " SET " . $campo . " = REPLACE(" . $campo . ",'�', 'n')";
	$db->query ($sql);
	$sql = "UPDATE " . $tabla . " SET " . $campo . " = REPLACE(" . $campo . ",'�', 'N')";
	$db->query ($sql);
	$sql = "UPDATE " . $tabla . " SET " . $campo . " = REPLACE(" . $campo . ",',', '')";
	$db->query ($sql);
	$sql = "UPDATE " . $tabla . " SET " . $campo . " = REPLACE(" . $campo . ",'.', '')";
	$db->query ($sql);
	$sql = "UPDATE " . $tabla . " SET " . $campo . " = REPLACE(" . $campo . ",'�', '-')";
	$db->query ($sql);
	$sql = "UPDATE " . $tabla . " SET " . $campo . " = REPLACE(" . $campo . ",'?', '')";
	$db->query ($sql);
	$sql = "UPDATE " . $tabla . " SET " . $campo . " = REPLACE(" . $campo . ",'�', '')";
	$db->query ($sql);
	$sql = "UPDATE " . $tabla . " SET " . $campo . " = REPLACE(" . $campo . ",'�', '')";
	$db->query ($sql);
	$sql = "UPDATE " . $tabla . " SET " . $campo . " = REPLACE(" . $campo . ",'!', '')";
	$db->query ($sql);
	$sql = "UPDATE " . $tabla . " SET " . $campo . " = REPLACE(" . $campo . ",'�', 'u')";
	$db->query ($sql);
	$sql = "UPDATE " . $tabla . " SET " . $campo . " = REPLACE(" . $campo . ",'�', 'u')";
	$db->query ($sql);
	$sql = "UPDATE " . $tabla . " SET " . $campo . " = REPLACE(" . $campo . ",'�', '\'')";
	$db->query ($sql);
}
?>