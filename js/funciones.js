function nuevo_libro(saga)
{
    $.ajax(
    {
        type : "POST",
        url : "cargarLibro.php",
        data : "idSaga=" + saga + "&retTotal=" + 1,
        success : function(data)
        {
            vex.dialog.open(
            {
                message : 'Agregar libro',
                input : [ data ].join(''),
                buttons : [ $.extend({}, vex.dialog.buttons.YES,
                {
                    text : 'Aceptar'
                }), $.extend({}, vex.dialog.buttons.NO,
                {
                    text : 'Cancelar'
                }) ],
                callback : function(data)
                {
                    // var dataString = data.serialize();
                    if (!data)
                    {
                        console.log('Cancelled')
                    }
                    else
                    {
                        $.ajax(
                        {
                            type : "POST",
                            url : "cargarLibro.php",
                            data : "&anioEdicion=" + data.anioEdicion + "&cantCapitulos=" + data.cantCapitulos
                                    + "&descripcion=" + data.descripcion + "&idSaga=" + data.idSaga + "&orden="
                                    + data.orden + "&retTotal=" + data.retTotal + "&titulo=" + data.titulo
                                    + "&darDeAlta=1",
                            success : function(data)
                            {
                                // if (data == 0)
                                // {
                                vex.dialog.open(
                                {
                                    message : 'SE HA HECHO EL ALTA DE FORMA CORRECTA',
                                    buttons : [ $.extend({}, vex.dialog.buttons.YES,
                                    {
                                        text : 'Aceptar',
                                        click : function(e)
                                        {
                                            location.href = "Total.php";
                                        }
                                    }) ]
                                });
                                // }
                                // else
                                // {
                                // if (data != 12)
                                // {
                                // vex.dialog.open(
                                // {
                                // message : data,
                                // buttons : [ $.extend({}, vex.dialog.buttons.YES,
                                // {
                                // text : 'Aceptar',
                                // click : function(e)
                                // {
                                // nuevacomp()
                                // }
                                // }) ]
                                // });
                                // }
                                // }
                            }
                        })

                    }
                }
            })
        }
    })
}

function nuevo_saga(autorId)
{
    $.ajax(
    {
        type : "POST",
        url : "cargarSaga.php",
        data : "autorId=" + autorId + "&retTotal=" + 1,
        success : function(data)
        {
            vex.dialog.open(
            {
                message : 'Agregar Saga',
                input : [ data ].join(''),
                buttons : [ $.extend({}, vex.dialog.buttons.YES,
                {
                    text : 'Aceptar'
                }), $.extend({}, vex.dialog.buttons.NO,
                {
                    text : 'Cancelar'
                }) ],
                callback : function(data)
                {
                    // var dataString = data.serialize();
                    if (!data)
                    {
                        console.log('Cancelled')
                    }
                    else
                    {
                        $.ajax(
                        {
                            type : "POST",
                            url : "cargarSaga.php",
                            data : "&titulo=" + data.titulo + "&idAutor=" + data.idAutor + "&cantLibros="
                                    + data.cantLibros + "&descripcion=" + data.descripcion + "&imagen=" + data.imagen
                                    + "&retTotal=" + data.retTotal + "&darDeAlta=" + data.darDeAlta,
                            success : function(data)
                            {
                                vex.dialog.open(
                                {
                                    message : 'SE HA HECHO EL ALTA DE FORMA CORRECTA',
                                    buttons : [ $.extend({}, vex.dialog.buttons.YES,
                                    {
                                        text : 'Aceptar',
                                        click : function(e)
                                        {
                                            location.href = "Total.php";
                                        }
                                    }) ]
                                });
                            }
                        })

                    }
                }
            })
        }
    })
}

function nuevo_capitulo(idSaga, autorId, idLibro)
{
    $.ajax(
    {
        type : "POST",
        url : "cargarCapitulo.php",
        data : "idSaga=" + idSaga + "&autorId=" + autorId + "&idLibro=" + idLibro + "&retTotal=" + 1,
        success : function(data)
        {
            vex.dialog.open(
            {
                message : 'Agregar Capitulo',
                input : [ data ].join(''),
                buttons : [ $.extend({}, vex.dialog.buttons.YES,
                {
                    text : 'Aceptar'
                }), $.extend({}, vex.dialog.buttons.NO,
                {
                    text : 'Cancelar'
                }) ],
                callback : function(data)
                {
                    // var dataString = data.serialize();
                    if (!data)
                    {
                        console.log('Cancelled')
                    }
                    else
                    {
                        $.ajax(
                        {
                            type : "POST",
                            url : "cargarCapitulo.php",
                            data : "&orden=" + data.orden + "&titulo=" + data.titulo + "&idAutor=" + data.idAutor
                                    + "&idSaga=" + data.idSaga + "&idLibro=" + data.idLibro + "&descripcion="
                                    + data.descripcion + "&capitulo=" + data.capitulo + "&Youtube=" + data.Youtube
                                    + "&retTotal=" + data.retTotal + "&darDeAlta=" + data.darDeAlta,
                            success : function(data)
                            {
                                vex.dialog.open(
                                {
                                    message : 'SE HA HECHO EL ALTA DE FORMA CORRECTA',
                                    buttons : [ $.extend({}, vex.dialog.buttons.YES,
                                    {
                                        text : 'Aceptar',
                                        click : function(e)
                                        {
                                            location.href = "Total.php";
                                        }
                                    }) ]
                                });
                            }
                        })

                    }
                }
            })
        }
    })
}

function nuevo_autor ()
{
    $.ajax(
    {
        type : "POST",
        url : "cargarAutor.php",
        data : "",
        success : function(data)
        {
            vex.dialog.open(
            {
                message : 'Carga de Autores',
                input : [ data ].join(''),
                buttons : [ $.extend({}, vex.dialog.buttons.YES,
                {
                    text : 'Aceptar'
                }), $.extend({}, vex.dialog.buttons.NO,
                {
                    text : 'Cancelar'
                }) ],
                callback : function(data)
                {
                    if (!data)
                    {
                        console.log('Cancelled')
                    }
                    else
                    {
                        
                        $.ajax(
                        {
                            type : "POST",
//                            contentType: "application/json",
                            url : "cargarAutor.php",
                            data : "&apellido=" + data.apellido + "&realname=" + data.realname
                                    + "&segname=" + data.segname + "&nac=" + data.nac + "&def="
                                    + data.def + "&nacion=" + data.nacion + "&notas=" + data.notas
                                    + "&file1=" + data.file1 + "&darDeAlta=" + data.darDeAlta,
                            success : function(data)
                            { 
                                vex.dialog.open(
                                {
                                    message : 'SE HA HECHO EL ALTA DE FORMA CORRECTA',
                                    buttons : [ $.extend({}, vex.dialog.buttons.YES,
                                    {
                                        text : 'Aceptar',
                                        click : function(e)
                                        {
                                            location.href = "Total.php";
                                        }
                                    }) ]
                                });
                            }
                        })

                    }
                }
            })
        }
    })
}