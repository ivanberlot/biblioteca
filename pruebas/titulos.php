<?php
/**
 * Mostramos los titulos que hay para una saga.
 *
 * @author iberlot <@> iberlot@usal.edu.ar
 *         @date 23 dic. 2015
 *         @lenguage PHP
 * @name titulos.php
 * @version 0.1 version inicial del archivo.
 * @package @project Mytthos
 */
ob_start ();

require_once ("config/includes.php");

$where = array ();
$parametros = array ();
$html = "";

if (isset ($_REQUEST["idSaga"]) and $_REQUEST["idSaga"] != "")
{
	$where[] = " idSaga = :idSaga ";
	$parametros[] = trim ($_REQUEST["idSaga"]);
}
else
{
	header ("Location:sagas.php?autorId=$idAutor");
	exit ();
}

if ($where != "")
{
	$where = implode (" AND ", $where);

	$where = " AND " . $where;
}

$sql = "SELECT * FROM Saga WHERE 1=1 " . $where;

if ($result = $db->query ($sql, $esParam = true, $parametros))
{
	if ($row = $db->fetch_array ($result))
	{
		$html .= "<div id='autor'>";
		$html .= '<img class="fotoAutor" src="fotosSagas/' . $row['imagen'] . '">';
		$html .= '<Br />';
		$html .= '<h4>' . $row['titulo'] . '</h4>';

		$html .= 'Cantidad de libros: ' . $row['cantLibros'];
		$html .= '<Br />';

		$html .= '<Br />';
		$html .= $row['descripcion'];
		$html .= '<Br />';

		$html .= "</div>";

		echo "<h3>Titulos</h3>\n";
	}
}

$where = array ();
$parametros = array ();

if (isset ($_REQUEST["idSaga"]) and $_REQUEST["idSaga"] != "")
{
	$where[] = " idSaga = :idSaga ";
	$parametros[] = trim ($_REQUEST["idSaga"]);
}
else
{
	header ("Location:sagas.php?autorId=$idAutor");
	exit ();
}

if (isset ($_REQUEST["idAutor"]) and $_REQUEST["idAutor"] != "")
{
	$where[] = " idAutor = :idAutor ";
	$parametros[] = trim ($_REQUEST["idAutor"]);
}
else
{
	header ("Location:autores.php");

	exit ();
}

if ($where != "")
{
	$where = implode (" AND ", $where);

	$where = " AND " . $where;
}

$sql = "SELECT * FROM Libro WHERE 1=1 " . $where . "ORDER BY ordenSaga";

if ($result = $db->query ($sql, $esParam = true, $parametros))
{
	while ($row = $db->fetch_array ($result))
	{
		$idSaga = trim ($row['idSaga']);

		$idLibro = $row['idLibro'];

		$titulo = trim ($row['titulo']);

		$ordenLibro = $row['ordenSaga'];

		$tituloLink = str_pad ($ordenLibro, 2, "0", STR_PAD_LEFT) . "-" . str_replace (' ', '_', $titulo);

		$html .= "<li><a href='capitulos.php?idLibro=$idLibro'>";

		$html .= "<b>&nbsp;$titulo</b></a></li><Br/>";
	}
}

echo $html;
?>


<Br/><Br/><Br/>
<Br/><Br/><Br/>