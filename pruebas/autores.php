<?php

/**
 * Encargado de mostrar el listado de autores
 *
 * @author iberlot <@> ivanberlot@gmail.com
 * @todo FechaC 23/12/2015 - Lenguaje PHP
 *
 * @name autores.php
 *
 * @version 0.1 - Version de inicio
 *
 * @package Mytthos
 *
 * @category General
 *
 * @link ../config/includes.php - Archivo con todos los includes del sistema
 */
/*
 * Querido programador:
 *
 * Cuando escribi este codigo, solo Dios y yo sabiamos como funcionaba.
 * Ahora, Solo Dios lo sabe!!!
 *
 * Asi que, si esta tratando de 'optimizar' esta rutina y fracasa (seguramente),
 * por favor, incremente el siguiente contador como una advertencia para el
 * siguiente colega:
 *
 * totalHorasPerdidasAqui = 5
 *
 */
require_once ("config/includes.php");

$parametros = array ();
$html = "";

if (isset ($_REQUEST["idAutor"]) and $_REQUEST["idAutor"] != "")
{
	$where[] = " idAutor = :idAutor ";
	$parametros[] = trim ($_REQUEST["idAutor"]);
}
if (isset ($_REQUEST["apellido"]) and $_REQUEST["apellido"] != "")
{
	$where[] = " apellido = :apellido ";
	$parametros[] = trim ($_REQUEST["apellido"]);
}
if (isset ($_REQUEST["nombre"]) and $_REQUEST["nombre"] != "")
{
	$where[] = " nombre = :nombre ";
	$parametros[] = trim ($_REQUEST["nombre"]);
}
if (isset ($_REQUEST["segNombre"]) and $_REQUEST["segNombre"] != "")
{
	$where[] = " segNombre = :segNombre ";
	$parametros[] = trim ($_REQUEST["segNombre"]);
}
if (isset ($_REQUEST["fechaNac"]) and $_REQUEST["fechaNac"] != "")
{
	$where[] = " fechaNac = :fechaNac ";
	$parametros[] = trim ($_REQUEST["fechaNac"]);
}
if (isset ($_REQUEST["fechaDec"]) and $_REQUEST["fechaDec"] != "")
{
	$where[] = " fechaDec = :fechaDec ";
	$parametros[] = trim ($_REQUEST["fechaDec"]);
}
if (isset ($_REQUEST["nacionalidad"]) and $_REQUEST["nacionalidad"] != "")
{
	$where[] = " nacionalidad = :nacionalidad ";
	$parametros[] = trim ($_REQUEST["nacionalidad"]);
}

if (isset ($where) and $where != "")
{
	$where = implode (" AND ", $where);

	$where = " AND " . $where;
}
else
{
	$where = "";
}
$html .= "<h3>Autores</h3>\n";

$directorio = "Biblioteca";

$archivo = scandir ($directorio);

$sql = "SELECT * FROM Autor WHERE 1=1 " . $where . "ORDER BY Apellido";

if ($result = $db->query ($sql))
{
	while ($row = $db->fetch_array ($result))
	{
		if ($row['segNombre'] != "")
		{
			$nombre = trim ($row['nombre']) . "_" . trim ($row['segNombre']);
			$nombreTitulo = trim ($row['nombre']) . " " . trim ($row['segNombre']);
		}
		else
		{
			$nombre = trim ($row['nombre']);
			$nombreTitulo = trim ($row['nombre']);
		}

		$nombre = str_replace (' ', '_', $nombre);

		$html .= "<li><a href='sagas.php?idAutor=" . $row['idAutor'] . "'>" . $row['apellido'] . ", " . $nombreTitulo . "</a></li><Br/>";
	}
}

echo $html;
?>

<Br />
<Br />
<Br />
<Br />
<Br />
<Br />