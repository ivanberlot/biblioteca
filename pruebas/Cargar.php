<?php
/**
 *
 * @author iberlot <@> iberlot@usal.edu.ar
 * @todo 13 mar. 2018
 *       @lenguage PHP
 * @name Cargar.php
 * @version 0.1 version inicial del archivo.
 * @package @project
 */

/*
 * Querido programador:
 *
 * Cuando escribi este codigo, solo Dios y yo sabiamos como funcionaba.
 * Ahora, Solo Dios lo sabe!!!
 *
 * Asi que, si esta tratando de 'optimizar' esta rutina y fracasa (seguramente),
 * por favor, incremente el siguiente contador como una advertencia para el
 * siguiente colega:
 *
 * totalHorasPerdidasAqui = 0
 *
 */
ob_start ();

require_once ("config/includes.php");

$html = "";

$pizza = $sitio->listar ('Biblioteca');

foreach ($pizza as $pepino)
{
	$where = array ();
	$parametros = array ();

	$porciones = explode ("/", $pepino);

	$autor = explode ("_", $porciones[0]);

	if (isset ($autor[0]) and $autor[0] != "")
	{
		$where[] = " apellido = :apellido ";
		$parametros[] = $autor[0];
	}
	if (isset ($autor[1]) and $autor[1] != "")
	{
		$where[] = " nombre = :nombre ";
		$parametros[] = trim ($autor[1]);
	}
	if (isset ($autor[2]) and $autor[2] != "")
	{
		$where[] = " segNombre = :segNombre ";
		$parametros[] = trim ($autor[2]);
	}

	if (isset ($where) and $where != "")
	{
		$where = implode (" AND ", $where);

		$where = " AND " . $where;
	}

	$sql = "SELECT * FROM Autor WHERE 1=1 " . $where . "ORDER BY Apellido";

	if ($result = $db->query ($sql, $esParam = true, $parametros))
	{
		if (!$row = $db->fetch_array ($result))
		{
			$campos2 = array ();

			$valores2 = array ();
			$parametros2 = array ();
			if (isset ($autor[0]) and $autor[0] != "")
			{
				$campos2[] = " apellido ";
				$valores2[] = ":apellido ";
				$parametros2[] = $autor[0];
			}
			if (isset ($autor[1]) and $autor[1] != "")
			{
				$campos2[] = " nombre ";
				$valores2[] = ":nombre ";
				$parametros2[] = $autor[1];
			}
			if (isset ($autor[2]) and $autor[2] != "")
			{
				$campos2[] = " segNombre ";
				$valores2[] = ":segNombre ";
				$parametros2[] = $autor[2];
			}

			if ($campos2 != "" and $valores2 != "")
			{
				$valores2 = implode (", ", $valores2);
				$campos2 = implode (", ", $campos2);
			}
			$sql2 = "INSERT INTO Autor (" . $campos2 . ") VALUES (" . $valores2 . ")";

			if ($db->query ($sql2, true, $parametros2))
			{
				print_r ($autor);
				print_r ("<Br>");
			}
		}
	}
}

?>