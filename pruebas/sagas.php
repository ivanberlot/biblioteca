<?php
/**
 * Muestra el listado de sagas para un autor X.
 *
 * @author iberlot <@> iberlot@usal.edu.ar
 *         @date 23 dic. 2015
 *         @lenguage PHP
 * @name sagas.php
 * @version 0.1 version inicial del archivo.
 * @package @project Mytthos
 */
ob_start ();

require_once ("config/includes.php");

$where = array ();
$parametros = array ();
$html = "";

if (isset ($_REQUEST["idAutor"]) and $_REQUEST["idAutor"] != "")
{
	$where[] = " idAutor = :idAutor ";
	$parametros[] = trim ($_REQUEST["idAutor"]);
}
else
{
	header ("Location:autores.php");

	exit ();
}

if ($where != "")
{
	$where = implode (" AND ", $where);

	$where = " AND " . $where;
}

$sql = "SELECT * FROM Autor WHERE 1=1 " . $where;

if ($result = $db->query ($sql, $esParam = true, $parametros))
{
	if ($row = $db->fetch_array ($result))
	{

		$html .= "<div id='autor'>";
		$html .= '<img class="fotoAutor" src="fotosAutores/' . $row['foto'] . '">';
		$html .= '<Br />';
		$html .= '<h4>' . $row['apellido'] . ", " . $row['nombre'] . " " . $row['segNombre'] . '</h4>';

		$html .= 'Fecha de nacimiento: ' . $row['fechaNac'];
		$html .= '<Br />';

		if ($row['fechaDec'] > 1)
		{
			$html .= 'Fecha de defuncion: ' . $row['fechaDec'];
			$html .= '<Br />';
		}

		$html .= '<Br />';
		$html .= $row['notas'];
		$html .= '<Br />';

		$html .= "</div>";
		$html .= "<h3>Sagas</h3>\n";

		$sql = "SELECT * FROM Saga WHERE 1=1 " . $where . " ORDER BY titulo";

		if ($result = $db->query ($sql, $esParam = true, $parametros))
		{
			while ($row = $db->fetch_array ($result))
			{

				$tituloLink = str_replace (' ', '_', trim ($row['titulo']));

				$html .= "<li><a href='titulos.php?idSaga=" . trim ($row['idSaga']) . "&idAutor=" . trim ($_REQUEST["idAutor"]) . "'>" . trim ($row['titulo']) . "</a></li><Br/>";
			}
		}
	}
}

echo $html;
?>

<Br />
<Br />
<Br />
<Br />
<Br />
<Br />