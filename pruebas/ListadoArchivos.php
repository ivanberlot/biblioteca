<?php
/**
 *
 * @author iberlot <@> iberlot@usal.edu.ar
 * @todo 7 feb. 2018
 *       @lenguage PHP
 * @name ListadoArchivos.php
 * @version 0.1 version inicial del archivo.
 * @package @project
 */

/*
 * Querido programador:
 *
 * Cuando escribi este codigo, solo Dios y yo sabiamos como funcionaba.
 * Ahora, Solo Dios lo sabe!!!
 *
 * Asi que, si esta tratando de 'optimizar' esta rutina y fracasa (seguramente),
 * por favor, incremente el siguiente contador como una advertencia para el
 * siguiente colega:
 *
 * totalHorasPerdidasAqui = 0
 *
 */
ob_start ();

require_once ("config/includes.php");

$rowArchivo = "";
?>
<head>
<script type="text/javascript">
			var abrirenVentanaNueva = 0;

			var tagApartado = 'a';
			var docActual = location.href;
			function iniciaMenu(menu){
				idMenu = menu
				menu = document.getElementById(menu);
				for(var m = 0; m < menu.getElementsByTagName('ul').length; m++){
					el = menu.getElementsByTagName('ul')[m]
					el.style.display = 'none';
					el.className = 'menuDoc';
					el.parentNode.className = 'cCerrada'
					textoNodo = el.parentNode.firstChild.nodeValue;
					nuevoNodo = document.createElement(tagApartado);
					if(tagApartado == 'a') nuevoNodo.href = '#' + textoNodo;
					nuevoNodo.className = 'tagApartado';
					nuevoNodo.appendChild(document.createTextNode(textoNodo));
					el.parentNode.replaceChild(nuevoNodo,el.parentNode.firstChild);
					nuevoNodo.onclick = function(){
						hijo = sacaPrimerHijo(this.parentNode, 'ul')
						hijo.style.display = hijo.style.display == 'none' ? 'block' : 'none';
						if(this.parentNode.className == 'cCerrada' || this.parentNode.className == 'cAbierta'){
							this.parentNode.className = this.parentNode.className == 'cCerrada' ? 'cAbierta' : 'cCerrada'
						}
						else{
							this.parentNode.className = this.parentNode.className == 'cAbiertaSeleccionada' ? 'cCerradaSeleccionada' : 'cAbiertaSeleccionada'
						}
						return false;
					}
				}
				documentoActual(idMenu)
			}
			function sacaPrimerHijo(obj, tag){
				for(var m = 0; m < obj.childNodes.length; m++){
					if(obj.childNodes[m].tagName && obj.childNodes[m].tagName.toLowerCase() == tag){
						return obj.childNodes[m];
						break;
					}
				}
			}
			function documentoActual(menu){
				idMenu = menu
				menu = document.getElementById(menu);
				for(var s = 0; s < menu.getElementsByTagName('a').length; s++){
					if(abrirenVentanaNueva) menu.getElementsByTagName('a')[s].target = 'blank';
					enlace = menu.getElementsByTagName('a')[s].href
					if(enlace == docActual){
						menu.getElementsByTagName('a')[s].parentNode.className = 'documentoActual'
					}
					if(enlace == docActual && menu.getElementsByTagName('a')[s].parentNode.parentNode.id != idMenu){
						menu.getElementsByTagName('a')[s].parentNode.parentNode.parentNode.className = 'cAbiertaSeleccionada'
						var enlaceCatPadre = sacaPrimerHijo(menu.getElementsByTagName('a')[s].parentNode.parentNode.parentNode, 'a')
						enlaceCatPadre.onclick = function(){
							hijo = sacaPrimerHijo(this.parentNode, 'ul')
							hijo.style.display = hijo.style.display == 'none' ? 'block' : 'none';
							this.parentNode.className = this.parentNode.className == 'cAbiertaSeleccionada' ? 'cCerradaSeleccionada' : 'cAbiertaSeleccionada'
							return false;

						}
						nodoSig = sacaPrimerHijo(menu.getElementsByTagName('a')[s].parentNode.parentNode.parentNode, 'ul')
						nodoSig.style.display = 'block';/**/
						abrePadre(idMenu, enlaceCatPadre.parentNode)
					}
				}
			}
			function abrePadre(idmenu, obj){
				obj.parentNode.parentNode.className = 'cAbiertaSeleccionada'
				var nodoSig = sacaPrimerHijo(obj, 'ul')
				nodoSig.style.display = 'block';
				if(obj.parentNode.id != idmenu){
					abrePadre(idmenu, obj.parentNode.parentNode)
				}
			}
		</script>

<script src="/Mytthos/inc/vex/dist/js/vex.combined.min.js"></script>
<script>vex.defaultOptions.className = 'vex-theme-os'</script>
<link rel="stylesheet" href="/Mytthos/css/Base.css" />
<link rel="stylesheet" href="/Mytthos/inc/vex/dist/css/vex.css" />
<link rel="stylesheet" href="/Mytthos/inc/vex/dist/css/vex-theme-os.css" />
</head>
<div id='content'>
    <div id='cuerpo'>
        <ul id='miMenu'>
					<?php
					listar_directorios_ruta ("Biblioteca/");

					function listar_directorios_ruta($ruta)
					{
						// abrir un directorio y listarlo recursivo
						if (is_dir ($ruta))
						{
							if ($dh = opendir ($ruta))
							{
								while (($file = readdir ($dh)) !== false)
								{
									// esta l�nea la utilizar�amos si queremos listar todo lo que hay en el directorio
									// mostrar�a tanto archivos como directorios
									// echo "<br>Nombre de archivo: $file : Es un: " . filetype($ruta . $file);
									if (is_dir ($ruta . $file) && $file != "." && $file != "..")
									{
										// solo si el archivo es un directorio, distinto que "." y ".."
										// echo "<br>Directorio: $ruta$file";
										echo "<li>Directorio: $file<ul>";

										listar_directorios_ruta ($ruta . $file . "/");

										echo "</ul>";
										echo "</li>";
									}
									elseif ($file != "." && $file != "..")
									{
										echo "<li>";
										echo $file;
										echo "</li>";
									}
								}
								closedir ($dh);
							}
						}
						else
						{
							echo "<br>No es ruta valida";
						}
					}
					?>
				</ul>

    </div>


    <!--end of cuerpo-->


</div>
<!--end of content-->
<script type="text/javascript">
<!--
iniciaMenu('miMenu');
// -->
</script>
<script src="/Mytthos/js/funciones.js"></script>
<?php
ob_end_flush ();
?>