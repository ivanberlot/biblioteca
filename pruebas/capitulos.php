<?php
/**
 * Encargado de mostrar el listado de capitulos.
 *
 * @author iberlot <@> iberlot@usal.edu.ar
 *         @date 23 dic. 2015
 *         @lenguage PHP
 * @name capitulos.php
 * @version 0.1 version inicial del archivo.
 * @package @project Mytthos
 */
ob_start ();

require_once ("config/includes.php");

$where = array ();
$parametros = array ();
$html = "";

if (isset ($_REQUEST["idLibro"]) and $_REQUEST["idLibro"] != "")
{
	$where[] = " idLibro = :idLibro ";
	$parametros[] = trim ($_REQUEST["idLibro"]);
}
else
{
	header ("Location:autores.php");

	exit ();
}

if ($where != "")
{
	$where = implode (" AND ", $where);

	$where = " AND " . $where;
}

$sql = "SELECT * FROM Libro WHERE 1 = 1 " . $where;

if ($result = $db->query ($sql, $esParam = true, $parametros))
{
	if ($row = $db->fetch_array ($result))
	{
		$html .= "<div id='autor'>";
		$html .= '<img class="fotoAutor" src="fotosLibros/' . $row['imagen'] . '">';
		$html .= '<Br />';
		$html .= '<h4>' . $row['titulo'] . '</h4>';

		$html .= 'Cantidad de capitulos: ' . $row['cantCap'];
		$html .= '<Br />';

		$html .= 'A&ntilde;o de edicion: ' . $row['anio'];
		$html .= '<Br />';

		$html .= '<Br />';
		$html .= html_entity_decode ($row['resenia']);
		$html .= '<Br />';

		$html .= "</div>";

		$codigoYouTube = $row['ListaDeRepr'];

		$html .= "<h3>Capitulos</h3>\n <div id='cuerpo' >\n";

		$sql = "SELECT * FROM Capitulo WHERE  1 = 1 " . $where . " ORDER BY nrOrden";

		if ($result = $db->query ($sql, $esParam = true, $parametros))
		{
			while ($row = $db->fetch_array ($result))
			{
				$tituloCapitulo = str_pad ($row['nrOrden'], 2, "0", STR_PAD_LEFT) . "-" . trim ($row['titulo']);

				$html .= "<a href='leer.php?idCapitulo=" . $row['idCapitulo'] . "'><b>&nbsp;" . $tituloCapitulo . "</b></a>";
				if ($_SESSION['estado'] == 'Iniciada')
				{
					$html .= "&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;&nbsp;";
					$html .= "<a href='editar.php?idCapitulo=" . $row['idCapitulo'] . "'><b>&nbsp;Edit</b></a>";
				}
				$html .= "<Br />";
			}

			// if ($codigoYouTube != "")
			// {
			// $html .= "<Br />";
			// $html .= '<Div align="center"><iframe width="200" height="113" src="https://www.youtube.com/embed/videoseries?list=' . $codigoYouTube . '" frameborder="0" allowfullscreen></iframe></Div>';
			// }
			$html .= "</div>";

			echo $html;
		}
	}
}
mysqli_close ($link);
?>