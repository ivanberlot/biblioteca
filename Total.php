<?php
/**
 * Modulo que muestra el arbol total de libros y da la posibilidad de su insercion.
 *
 * @author iberlot <@> ivanberlot@gmail.com
 * @since 29/09/2016
 * @lenguage PHP
 *
 * @name Total.php
 *
 * @version 0.1 - Version de inicio
 *          0.2 - Las cargas se realizan por medio de vex dialog.
 *
 * @link Config/includes.php - Archivo con todos los includes del sistema
 */
/*
 * Querido programador:
 *
 * Cuando escribi este codigo, solo Dios y yo sabiamos como funcionaba.
 * Ahora, Solo Dios lo sabe!!!
 *
 * Asi que, si esta tratando de 'optimizar' esta rutina y fracasa (seguramente),
 * por favor, incremente el siguiente contador como una advertencia para el
 * siguiente colega:
 *
 * totalHorasPerdidasAqui = 61
 *
 */
ob_start ();

require_once ("config/includes.php");
?>
<head>
<script src="config/funciones.js"></script>

<style type="text/css">
<!--
#menu1 {
	font-size: 90%;
	list-style-image: url(/Mytthos/inc/doct.gif);
	border: 1px solid #006699;
	width: 200px;
	padding-top: .5em;
	padding-bottom: .5em;
	list-style: inside;
}

html body #menu1 {
	list-style: outside;
}

.cAbierta {
	list-style-type: none;
	list-style-image: url(/Mytthos/inc/carpabiertat.gif);
}

.cAbiertaSeleccionada {
	list-style-type: none;
	list-style-image: url(/Mytthos/inc/carpabiertasel.gif);
}

.cCerradaSeleccionada {
	list-style-type: none;
	list-style-image: url(/Mytthos/inc/carpcerradasel.gif);
}

.cCerrada {
	list-style-type: none;
	list-style-image: url(/Mytthos/inc/carpcerradat.gif);
}

.menuDoc {
	list-style-type: none;
	list-style-image: url(/Mytthos/inc/doct.gif);
}

.tagApartado, a.tagApartado {
	cursor: pointer;
	font-weight: bold;
	text-decoration: none;
}

.documentoActual {
	list-style-image: url(/Mytthos/inc/docsel.gif);
}

#menu1 a {
	color: #006699;
}

.documentoActual a {
	color: #993300;
}
-->
</style>
<script src="/Mytthos/inc/vex/dist/js/vex.combined.min.js"></script>
<script>vex.defaultOptions.className = 'vex-theme-os'</script>
<link rel="stylesheet" href="/Mytthos/inc/vex/dist/css/vex.css" />
<link rel="stylesheet" href="/Mytthos/inc/vex/dist/css/vex-theme-os.css" />
</head>
<div id='content'>
    <div id='cuerpo'>
        <ul id='miMenu'>
					<?php
					$parametros = array ();

					$sql = "SELECT Autor.* FROM Autor JOIN Libro ON Autor.idAutor = Libro.idAutor JOIN Capitulo ON Libro.idLibro = Capitulo.idLibro GROUP BY Autor.idAutor ORDER BY apellido";
					if ($result = $db->query ($sql, true, $parametros))
					{
						while ($row = $db->fetch_array ($result))
						{
							$lines = "";
							$lines2 = "";
							$idCapitulo = "";
							$hayCapitulo = "";

							$nombre = trim ($row['apellido']) . "_" . trim ($row['nombre']);
							$nombreTitulo = trim ($row['apellido']) . ", " . trim ($row['nombre']);

							if ($row['segNombre'] != "")
							{
								$nombre = $nombre . "_" . trim ($row['segNombre']);
								$nombreTitulo = $nombreTitulo . " " . trim ($row['segNombre']);
							}

							$nombre = str_replace (' ', '_', $nombre);

							$idAutor = $row['idAutor'];

							$lines .= "<!--" . substr ($nombreTitulo, 0, 50) . "-->";
							$lines .= "<li>" . $nombreTitulo;
							if (isset ($_SESSION['estado']) and $_SESSION['estado'] == 'Iniciada')
							{
								// echo " <a href='cargarSaga.php?autorId=" . $idAutor . "&retTotal=1' >Agregar Saga</a>";
								$lines .= "   <a onclick='nuevo_saga(" . $idAutor . ")'>Agregar Saga</a>";
							}
							$lines .= "<ul>";

							/*
							 * A Partir de aca realizamos la carga de las sagas
							 */

							$sqlSagas = "SELECT * FROM Saga WHERE idAutor = :idAutor"; // ORDER BY titulo";
							                                                           // $resultSagas = mysqli_query ($link, $sqlSagas) or die ('Query error: ' . mysqli_error ($link));
							                                                           // while ($rowSaga = mysqli_fetch_array ($resultSagas, MYSQLI_ASSOC))

							$parametros = array ();
							$parametros[] = trim ($idAutor);

							$resultSagas = $db->query ($sqlSagas, $esParam = true, $parametros);

							while ($rowSaga = $db->fetch_array ($resultSagas))
							{

								$idCapitulo = "";

								$idSaga = trim ($rowSaga["idSaga"]);

								$tituloSaga = trim ($rowSaga["titulo"]);

								$tituloLink = str_replace (' ', '_', $tituloSaga);

								$lines2 .= "<!-- Saga $tituloSaga -->";
								$lines2 .= "<li>" . $tituloSaga;
								if (isset ($_SESSION['estado']) and $_SESSION['estado'] == 'Iniciada')
								{
									// echo " <a href='cargarLibro.php?idSaga=" . $idSaga . "&retTotal=1' >Agregar Libro</a>";
									$lines2 .= "   <a onclick='nuevo_libro(" . $idSaga . ")'>Agregar Libro</a>";
								}
								$lines2 .= "<ul>";

								$sqlLibro = "SELECT * FROM Libro WHERE idAutor = :idAutor and idSaga = :idSaga ORDER BY ordenSaga";
								// $resultLibro = mysqli_query ($link, $sqlLibro) or die ('Query error: ' . mysqli_error ($link));

								$parametros = array ();
								$parametros[] = trim ($idAutor);
								$parametros[] = trim ($idSaga);

								$resultLibro = $db->query ($sqlLibro, $esParam = true, $parametros);

								// while ($rowLibro = mysqli_fetch_array ($resultLibro, MYSQLI_ASSOC))
								// {

								while ($rowLibro = $db->fetch_array ($resultLibro))
								{
									$idLibro = $rowLibro["idLibro"];

									$tituloLibro = trim ($rowLibro["titulo"]);

									$ordenLibro = $rowLibro["ordenSaga"];

									$tituloLibroLink = str_pad ($ordenLibro, 2, "0", STR_PAD_LEFT) . "-" . str_replace (' ', '_', $tituloLibro);

									$lines2 .= "<!-- Saga $tituloLibro -->";
									$lines2 .= "<li>" . str_pad ($ordenLibro, 2, "0", STR_PAD_LEFT) . "-" . $tituloLibro;
									if (isset ($_SESSION['estado']) and $_SESSION['estado'] == 'Iniciada')
									{
										// echo " <a id="opener">Agregar Capitulo</a>";
										// echo " <a href='cargarCapitulo.php?idSaga=" . $idSaga . "&autorId=" . $idAutor . "&idLibro=" . $idLibro . "&retTotal=1' >Agregar Capitulo</a>";
										$lines2 .= "   <a onclick='nuevo_capitulo(" . $idSaga . ", " . $idAutor . "," . $idLibro . ")' >Agregar Capitulo</a>";
									}
									$lines2 .= "<ul>";

									$sqlCapitulo = "SELECT * FROM Capitulo WHERE idLibro = :idLibro ORDER BY nrOrden";
									// $resultCapitulo = mysqli_query ($link, $sqlCapitulo) or die ('Query error: ' . mysqli_error ($link));
									$parametros = array ();
									$parametros[] = trim ($idLibro);

									$resultCapitulo = $db->query ($sqlCapitulo, $esParam = true, $parametros);

									// while ($rowCapitulo = mysqli_fetch_array ($resultCapitulo, MYSQLI_ASSOC))
									// {

									while ($rowCapitulo = $db->fetch_array ($resultCapitulo))
									{

										$idCapitulo = $rowCapitulo['idCapitulo'];
										$hayCapitulo = "Ok";

										$tituloCapitulo = str_pad ($rowCapitulo['nrOrden'], 2, "0", STR_PAD_LEFT) . "-" . trim ($rowCapitulo['titulo']);

										$lines2 .= "<li>";
										$lines2 .= "<a href='leer.php?idCapitulo=$idCapitulo'>
										<b>&nbsp;$tituloCapitulo</b>
										</a>";
										if (comprobarAudio ($db, $rowCapitulo['idCapitulo']))
										{
											$lines2 .= "<i class='fa fa-music' aria-hidden='true'></i>";
										}

										if (isset ($_SESSION['estado']) and $_SESSION['estado'] == 'Iniciada')
										{
											$lines2 .= "&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;&nbsp;
										<a href='editar.php?idCapitulo=$idCapitulo'>
										<b>&nbsp;Edit</b>
										</a>";
										}
										$lines2 .= "</li>";
									}

									$lines2 .= "</ul></li>";
								}
								$lines2 .= "</ul></li>";

								if (isset ($idCapitulo) and $idCapitulo != "")
								{
									$lines .= $lines2;
								}
							}
							$lines .= "</ul>";
							$lines .= "</li>";

							// if (isset ($idCapitulo) and $idCapitulo != "")
							if (isset ($hayCapitulo) and $hayCapitulo != "")
							{
								echo $lines;
							}
						}
					}
					?>
				</ul>

        <a onclick='nuevo_autor()'>Agregar Autor</a>
    </div>


    <!--end of cuerpo-->


</div>
<!--end of content-->
<script type="text/javascript">
<!--
iniciaMenu('miMenu');
// -->
</script>
<script src="/Mytthos/js/funciones.js"></script>
<?php
ob_end_flush ();
?>