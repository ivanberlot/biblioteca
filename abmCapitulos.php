<?php
/**
 * Archivo que muestra el listado de capitulos de un libro y las opciones de alta baja y modificacion en caso de ser nesesarias.
 *
 * @author iberlot <@> iberlot@usal.edu.ar
 * @since 22 may. 2018
 * @lenguage PHP
 * @name abmSagas.php
 * @version 0.1 version inicial del archivo.
 */

/*
 * Querido programador:
 *
 * Cuando escribi este codigo, solo Dios y yo sabiamos como funcionaba.
 * Ahora, Solo Dios lo sabe!!!
 *
 * Asi que, si esta tratando de 'optimizar' esta rutina y fracasa (seguramente),
 * por favor, incremente el siguiente contador como una advertencia para el
 * siguiente colega:
 *
 * totalHorasPerdidasAqui = 10
 *
 */
ob_start ();

require_once ("config/includes.php");

if (isset ($_REQUEST['idLibro']) and $_REQUEST['idLibro'] != "")
{

	$libro = Array ();
	$libro['idLibro'] = trim ($_REQUEST['idLibro']);
	$libro = get_Libro ($db, $libro);

	$saga = Array ();
	$saga['idSaga'] = $libro['idSaga'];
	$saga = get_Saga ($db, $saga);

	$autor = Array ();
	$autor['idAutor'] = $libro['idAutor'];
	$autor = get_Autor ($db, $autor);

	$libro['titulo'] = trim ($libro['titulo']);
	$tituloCarpeta = str_replace (' ', '_', $libro['titulo']);
	$ordenLibro = $libro['ordenSaga'];

	$saga['titulo'] = trim ($saga['titulo']);
	$tituloCarpetaSaga = str_replace (' ', '_', $saga['titulo']);

	$apellido = trim ($autor['apellido']);
	$realname = trim ($autor['nombre']);
	$segname = trim ($autor['segNombre']);

	if ($segname != "")
	{
		$direCarpeta = $apellido . "_" . $realname . "_" . $segname;
	}
	else
	{
		$direCarpeta = $apellido . "_" . $realname;
	}

	$Carpeta = "Biblioteca/" . $direCarpeta . "/" . $tituloCarpetaSaga . "/" . str_pad ($ordenLibro, 2, "0", STR_PAD_LEFT) . "-" . $tituloCarpeta . "/Capitulos";

	// print_r($Carpeta);

	$sql = "SELECT MAX(nrOrden) nrOrden FROM Capitulo WHERE idLibro = :idLibro ";
	$parametros = array ();
	$parametros[] = trim ($_REQUEST['idLibro']);
	$resultCapitulo = $db->query ($sql, $esParam = true, $parametros);
	$rowCapitulo = $db->fetch_array ($resultCapitulo);
	$orden = $rowCapitulo['nrOrden'] + 1;
}

$abm = new class_abm ();

$abm->tabla = "Capitulo";
$abm->registros_por_pagina = 40;
$abm->campoId = "idCapitulo";
$abm->campoIdEsEditable = false;
$abm->orderByPorDefecto = "nrOrden";

$abm->redireccionarDespuesUpdate = "renameFold.php?tipo=capitulo&accion=2&id=%d";
$abm->redireccionarDespuesInsert = "renameFold.php?tipo=capitulo&accion=1&id=%d";
$abm->redireccionarDespuesDelete = "renameFold.php?tipo=capitulo&accion=3&id=%d";

if (isset ($_REQUEST['idLibro']) and $_REQUEST['idLibro'] != "")
{
	$abm->adicionalesSelect = " AND idLibro = " . $_REQUEST['idLibro'] . " ";
}

if (!isset ($_SESSION['id_Ususario']) or $_SESSION['id_Ususario'] == '')
{
	$_SESSION['id_Ususario'] = 0;
}
if (isset ($_SESSION['nivel']) and $_SESSION['nivel'] == '666')
{
	$abm->mostrarNuevo = true;
	$abm->mostrarBorrar = true;
	$abm->mostrarEditar = true;
}
else
{
	$abm->mostrarNuevo = false;
	$abm->mostrarBorrar = false;
	$abm->mostrarEditar = false;
}
$abm->busquedaTotal = true;

// $abm->iconoAgregar = '<a onclick="agregarConcepto()"><img src="/img/add.png"></img></a>';
// $abm->iconoEditar = '<a onclick="editarConcepto({id})"><img src="/img/editar.gif"></img></a>';

$abm->campos = array (
		array (
				'campo' => 'idCapitulo',
				'tipo' => 'texto',
				'exportar' => true,
				'titulo' => 'ID',
				'noEditar' => true,
				'noListar' => true,
				'buscar' => true
		),
		array (
				'campo' => 'Archivo',
				'tipo' => 'upload',
				'directorio' => 'Capitulos',
				'cargarEnBase' => TRUE,
				'grabarSinExtencion' => TRUE,
				'ubicacionArchivo' => $Carpeta,
				'nombreArchivo' => " \$data=str_pad (intval({{nrOrden}}), 2, '0', STR_PAD_LEFT) . '-' . str_replace (' ', '_', ucfirst(strtolower({{titulo}})));",
				'mostrar' => true,
				'exportar' => true,
				'titulo' => 'Archivo',
				'noMostrar' => true,
				'buscar' => true
		),
		array (
				'campo' => 'idLibro',
				'tipo' => 'dbCombo',
				'campoValor' => 'idLibro',
				'campoTexto' => 'titulo',
				'joinTable' => 'Libro',
				'joinCondition' => 'LEFT',
				'titulo' => 'Libro',
				'customOrder' => 'titulo',
				'incluirOpcionVacia' => FALSE,
				'noListar' => true,
				'valorPredefinido' => $_REQUEST['idLibro']
		),
		// array (
		// 'campo' => 'ArchivoOtro ',
		// 'tipo' => 'upload',
		// 'directorio' => 'fotosLibro',
		// 'alto' => '90',
		// 'ancho' => '90',
		// 'mostrar' => true,
		// 'exportar' => true,
		// 'titulo' => 'FOTO',
		// 'buscar' => true
		// ),
		array (
				'campo' => 'nrOrden',
				'cantidadDecimales' => '0',
				'valorPredefinido' => $orden,
				'tipo' => 'numero',
				'exportar' => true,
				'titulo' => 'ORDEN',
				'buscar' => true
		),
		array (
				'campo' => 'titulo',
				'tipo' => 'texto',
				'exportar' => true,
				'titulo' => 'TITULO',
				'customEvalListado' => 'echo "<td>".$fila[\'titulo\']; if(comprobarAudio($db,$fila[\'ID\'])){ echo "<i class=\'fa fa-music\' aria-hidden=\'true\'></i>";} echo "</td>";',
				'buscar' => true
		),
		array (
				'campo' => "",
				'tipo' => "texto",
				'titulo' => "Leer",
				'maxLen' => 30,
				'customPrintListado' => "<a href='leer.php?idCapitulo={id}' title='Leer'><i class='fa fa-book' aria-hidden='true'></i></a>",
				'centrarColumna' => true,
				'buscar' => false,
				'noEditar' => true,
				'noNuevo' => true
		),

		array (
				'campo' => "",
				'tipo' => "texto",
				'titulo' => "Editar Archivo",
				'maxLen' => 30,
				'customEvalListado' => 'echo "<td align=\"center\">"; if((isset ($_SESSION[\'estado\']) and $_SESSION[\'estado\'] == \'Iniciada\')){echo "<a href=\'editar.php?idCapitulo=$fila[ID]\' title=\'Leer\'><i class=\'fa fa-envira\' aria-hidden=\'true\'></i></a>";} echo "</td>";',
				// 'customPrintListado' => "<a href='editar.php?idCapitulo={id}' title='Leer'><i class='fa fa-envira' aria-hidden='true'></i></a>",
				'buscar' => false,
				'noEditar' => true,
				'noNuevo' => true
		),

		array (
				'campo' => "",
				'tipo' => "texto",
				'titulo' => "Subir audio",
				'maxLen' => 30,
				'customEvalListado' => 'echo "<td align=\"center\">"; if((isset ($_SESSION[\'estado\']) and $_SESSION[\'estado\'] == \'Iniciada\')){echo "<a href=\'subirAudio.php?idCapitulo=$fila[ID]\' title=\'Subir audio\'><i class=\'fa fa-music\' aria-hidden=\'true\'></i></a>";} echo "</td>";',
				// 'customPrintListado' => "<a href='editar.php?idCapitulo={id}' title='Leer'><i class='fa fa-envira' aria-hidden='true'></i></a>",
				'buscar' => false,
				'noEditar' => true,
				'noNuevo' => true
		),

		array (
				'campo' => "idCapitulo",
				'joinTable' => "Leidos",
				'campoTexto' => "idCapitulo",
				'campoValor' => "idCapitulo",
				'joinCondition' => "LEFT",
				'tipo' => "combo",
				'titulo' => "Visto",
				'compareMasJoin' => "Leidos.id_Ususario = $_SESSION[id_Ususario] ",
				'customEvalListado' => 'echo "<td align=\"center\">"; if($valor!=""){echo "<i class=\'fa fa-eye\' aria-hidden=\'true\' style=\'color:green\'></i>";}else{echo "<i class=\'fa fa-eye-slash\' aria-hidden=\'true\'></i>";}; echo "</td>";',
				'buscar' => false,
				'noEditar' => true,
				'noNuevo' => true
		)
);

if (!isset ($_REQUEST['abm_exportar']))
{
	?>
<!-- Estilos -->
<style>
#cuerpo {
	width: 95%;
}
</style>
<link rel="stylesheet" type="text/css" href="classes/cssABM/abm.css" />
<div id='content'>
    <div id="separadorh"></div>
    <h3 align="center"><?php
	// print $Titulo ?></h3>
    <div id="separadorh"></div>
    <div id='cuerpo' align='center'>
        <div id="separadorh"></div>
<?php
}

$abm->generarAbm ("", $libro['titulo']);

if (!isset ($_REQUEST['abm_exportar']))
{
	?>
        <p>&nbsp;</p>
        <p>
            <a href='abmLibros.php?idSaga=<?php

	echo $libro['idSaga'];
	?>&idAutor=<?php

	echo $libro['idAutor'];
	?>'>Volver al Menu Anterior</a>
        </p>
        <p>&nbsp;</p>
    </div>
</div>
<BR />
<BR />
<BR />
<BR />
<BR />
</body>
</html>
<?php
}

ob_end_flush ();

?>
<!-- <script src="classes/jsABM/jam.stickytableheaders.js"></script> -->
<!-- <script> -->
<!-- 	$("table").stickyTableHeaders(); -->
<!-- </script> -->