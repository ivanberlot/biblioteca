<?php
/**
 * ABM de los autores, ademas debe encargarse de renombrar los directorios cuando corresponda.
 *
 * Muetra un listado general de los autores y da la posibilidad de eliminr crear o editar.
 * Al momento de hacer la edicion dispara un proceso que busca todas las combinaciones posibles del nombre a nivel directorios y los unifica.
 *
 *
 * @author iberlot <@> iberlot@usal.edu.ar
 * @since 16 mar. 2018
 *       @lenguage PHP
 * @name abmAutores.php
 * @version 0.1 version inicial del archivo.
 * @package @project
 */

/*
 * Querido programador:
 *
 * Cuando escribi este codigo, solo Dios y yo sabiamos como funcionaba.
 * Ahora, Solo Dios lo sabe!!!
 *
 * Asi que, si esta tratando de 'optimizar' esta rutina y fracasa (seguramente),
 * por favor, incremente el siguiente contador como una advertencia para el
 * siguiente colega:
 *
 * totalHorasPerdidasAqui = 0
 *
 */
ob_start ();

require_once ("config/includes.php");

$abm = new class_abm ();

// $abm->dbLink = $db_link;
$abm->tabla = "Autor";
$abm->registros_por_pagina = 20;
$abm->campoId = "idAutor";
$abm->campoIdEsEditable = false;
$abm->orderByPorDefecto = "apellido";

if (isset ($_SESSION['estado']) and $_SESSION['estado'] == 'Iniciada')
{
	$abm->mostrarNuevo = true;
	$abm->mostrarBorrar = true;
	$abm->mostrarEditar = true;
}
else
{
	$abm->mostrarNuevo = false;
	$abm->mostrarBorrar = false;
	$abm->mostrarEditar = false;
}
$abm->busquedaTotal = true;

$abm->redireccionarDespuesUpdate = "renameFold.php?tipo=autor&accion=2&id=%d";
$abm->redireccionarDespuesInsert = "renameFold.php?tipo=autor&accion=1&id=%d";
$abm->redireccionarDespuesDelete = "renameFold.php?tipo=autor&accion=3&id=%d";

// $abm->iconoAgregar = '<a onclick="agregarConcepto()"><img src="/img/add.png"></img></a>';
// $abm->iconoEditar = '<a onclick="editarConcepto({id})"><img src="/img/editar.gif"></img></a>';

$abm->campos = array (
		array (
				'campo' => 'idAutor',
				'tipo' => 'texto',
				'exportar' => true,
				'titulo' => 'ID',
				'noEditar' => true,
				'buscar' => true
		),
		array (
				'campo' => 'foto',
				'tipo' => 'upload',
				'directorio' => 'fotosAutores',
				'alto' => '90',
				'ancho' => '90',
				'cargarEnBase' => TRUE,
				'ubicacionArchivo' => 'fotosAutores',
				'nombreArchivo' => " \$data=str_replace (' ', '_', {{apellido}}).'_'.str_replace (' ', '_', {{nombre}}).'.jpg';",
				'mostrar' => true,
				'exportar' => true,
				'titulo' => 'FOTO',
				'buscar' => true
		),
		array (
				'campo' => 'apellido',
				'tipo' => 'texto',
				'exportar' => true,
				'titulo' => 'APELLIDO',
				'buscar' => true
		),
		array (
				'campo' => 'nombre',
				'tipo' => 'texto',
				'exportar' => true,
				'titulo' => 'NOMBRE',
				'buscar' => true
		),
		array (
				'campo' => 'segNombre',
				'tipo' => 'texto',
				'exportar' => true,
				'titulo' => 'SEGNOMBRE',
				'buscar' => true
		),
		array (
				'campo' => 'fechaNac',
				'tipo' => 'fecha',
				'exportar' => true,
				'titulo' => 'FECHANAC',
				'buscar' => true
		),
		array (
				'campo' => 'fechaDec',
				'tipo' => 'fecha',
				'exportar' => true,
				'titulo' => 'FECHADEC',
				'buscar' => true
		),
		array (
				'campo' => 'nacionalidad',
				'tipo' => 'dbCombo',
				'campoValor' => 'idGentilicio',
				'campoTexto' => 'nombreGentilicio',
				'joinTable' => 'gentilicios',
				'joinCondition' => 'LEFT',
				'titulo' => 'NACIONALIDAD',
				'customOrder' => 'nombreGentilicio',
				'incluirOpcionVacia' => FALSE
		),
		array (
				'campo' => 'notas',
				'tipo' => 'textarea',
				'tmostrar' => 100,
				'maxLen' => 2000,
				'exportar' => true,
				'titulo' => 'NOTAS',
				'buscar' => true
		),
		array (
				'campo' => "",
				'tipo' => "texto",
				'titulo' => "SAGAS",
				'maxLen' => 30,
				'customPrintListado' => "<a href='abmSagas.php?idAutor={id}' title='Ver Sagas'>Ver Sagas</a>",
				'buscar' => false,
				'noEditar' => true,
				'noNuevo' => true
		)
);

if (!isset ($_REQUEST['abm_exportar']))
{
	?>
<!-- Estilos -->
<style>
#cuerpo {
	width: 95%;
}
</style>
<link rel="stylesheet" type="text/css" href="classes/cssABM/abm.css" />
<div id='content'>
    <div id="separadorh"></div>
    <h3 align="center"><?php
	// print $Titulo ?></h3>
    <div id="separadorh"></div>
    <div id='cuerpo' align='center'>
        <div id="separadorh"></div>
<?php
}

$abm->generarAbm ("", "Autores");

if (!isset ($_REQUEST['abm_exportar']))
{
	?>
        <p>&nbsp;</p>
        <p>
            <a href='index.php'>Volver al Menu Anterior</a>
        </p>
        <p>&nbsp;</p>
    </div>
</div>
<BR />
<BR />
<BR />
<BR />
<BR />
</body>
</html>
<?php
}

ob_end_flush ();

?>
<!-- <script src="classes/jsABM/jam.stickytableheaders.js"></script> -->
<!-- <script> -->
<!-- 	$("table").stickyTableHeaders(); -->
<!-- </script> -->