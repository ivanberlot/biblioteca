<?php
/**
 *
 * @author iberlot
 *         Validar usuario
 *         mixed by Marcexl
 * @version 23112015
 * @package ValidacionDeUsuarios
 *
 * @link configs/includes.php - Archivo con todos los includes del sistema
 *
 *       Consulta a la BD si tiene persmisos
 */
session_start ();
ob_start ();
include_once 'config.php';

$token = $_SESSION['token'];
$validate = $_SESSION['validate'];

if ($token !== $validate)
{
	// header ("Location:" . $directorio . "error.php?notoken");
	exit ("no token");
}

// Registro el acceso del usuario en el appadmusu.aplicationlog
// Recuperamos la Ip desde la que se esta conectando el usuario
if (!empty ($_SERVER['HTTP_CLIENT_IP']))
{
	$ipUsuario = $_SERVER['HTTP_CLIENT_IP'];
}
elseif (!empty ($_SERVER['HTTP_X_FORWARDED_FOR']))
{
	$ipUsuario = $_SERVER['HTTP_X_FORWARDED_FOR'];
}
else
{
	$ipUsuario = $_SERVER['REMOTE_ADDR'];
}

// Recuperamos la direccion desde la que el usuario accedio a esta pagina
$actual_link = $_SERVER['REQUEST_URI'];

/* FILTRO1 averiguamos si la cuenta esta en la tabla */
$sqlUsrs = "SELECT * FROM Usuarios WHERE mailUsuario = :mailUsuario";

$parametros = array ();
$parametros[] = trim ($_SESSION['mailUsuario']);

$result = $db->query ($sqlUsrs, $esParam = true, $parametros);

if ($row = $db->fetch_array ($result))
{
	if ($_SESSION['mailUsuario'] == $row["mailUsuario"])
	{
		$_SESSION['id_Ususario'] = $row["id"];
		$_SESSION['cuenta'] = $row["nombreUsuario"];
		// $_SESSION['nivel'] = $row["nivel"];

		exit ();
		header ("Location:http://pepinos.ddns.net:2550/Mytthos/");
	}
}
else
{

	$sql = "INSERT INTO Usuarios (nombreUsuario, mailUsuario, nivel) VALUES (:nombreUsuario, :mailUsuario, :nivel)";

	$parametros = array ();
	$parametros[] = trim ($_SESSION['cuenta']);
	$parametros[] = trim ($_SESSION['mailUsuario']);
	$parametros[] = 1;

	$db->query ($sql, $esParam = true, $parametros);

	$_SESSION['nivel'] = 1;
	// $sqlUsrs2 = "INSERT
	// INTO appadmusu.aplicacionlog (idaplicacion, idmodulo, person, acceso, ip, data)
	// VALUES (:idaplicacion, :idmodulo, :person, SYSDATE, :ip,: data)";

	// $date_url = "SINPERMISOS";
	// $stmt2 = oci_parse ($linkOracle2, $sqlUsrs2);

	// // print "---".sqlUsrs2;
	// oci_bind_by_name ($stmt2, ":idaplicacion", $IDAPLICACION);
	// oci_bind_by_name ($stmt2, ":idmodulo", $IDMODULO);
	// oci_bind_by_name ($stmt2, ":person", $person);
	// oci_bind_by_name ($stmt2, ":ip", $ipUsuario);
	// oci_bind_by_name ($stmt2, ":data", $data_url);

	// oci_execute ($stmt2);

	/* si es invalido alguno de los filtros */

	// print_r ($_SESSION);
	// exit ("no cuenta");
}

// include ("/web/html/inc/footer.php"); // incluimo
ob_end_flush ();
?>

