<?php
/**
 * Alta Baja y Modificacion de paises y sus respectivos Gentilicios.
 *
 * @author iberlot <@> iberlot@usal.edu.ar
 * @todo 16 mar. 2018
 *       @lenguage PHP
 * @name abmPaises.php
 * @version 0.1 version inicial del archivo.
 * @package @project
 */

/*
 * Querido programador:
 *
 * Cuando escribi este codigo, solo Dios y yo sabiamos como funcionaba.
 * Ahora, Solo Dios lo sabe!!!
 *
 * Asi que, si esta tratando de 'optimizar' esta rutina y fracasa (seguramente),
 * por favor, incremente el siguiente contador como una advertencia para el
 * siguiente colega:
 *
 * totalHorasPerdidasAqui = 0
 *
 */
ob_start ();

require_once ("config/includes.php");

$abm = new class_abm ();

// $abm->dbLink = $db_link;
$abm->tabla = "gentilicios";
$abm->registros_por_pagina = 20;
$abm->campoId = "idGentilicio";
$abm->campoIdEsEditable = false;
$abm->campoOrder = "Pais DESC";
// $abm->mostrarNuevo = false;
// $abm->mostrarBorrar = false;
// $abm->mostrarEditar = false;
$abm->busquedaTotal = true;

// $abm->iconoAgregar = '<a onclick="agregarConcepto()"><img src="/img/add.png"></img></a>';
// $abm->iconoEditar = '<a onclick="editarConcepto({id})"><img src="/img/editar.gif"></img></a>';

$abm->campos = array (
		array (
				'campo' => 'idGentilicio',
				'tipo' => 'texto',
				'exportar' => true,
				'titulo' => 'ID',
				'noEditar' => true,
				'buscar' => true
		),
		array (
				'campo' => 'Pais',
				'tipo' => 'texto',
				'exportar' => true,
				'titulo' => 'Pais',
				'buscar' => true
		),
		array (
				'campo' => 'nombreGentilicio',
				'tipo' => 'texto',
				'exportar' => true,
				'titulo' => 'NOMBRE',
				'buscar' => true
		)
);

if (!isset ($_REQUEST['abm_exportar']))
{
	?>
<!-- Estilos -->
<style>
#cuerpo {
	width: 95%;
}
</style>
<link rel="stylesheet" type="text/css" href="classes/cssABM/abm.css" />
<div id='content'>
    <div id="separadorh"></div>
    <h3 align="center"><?php
	// print $Titulo ?></h3>
    <div id="separadorh"></div>
    <div id='cuerpo' align='center'>
        <div id="separadorh"></div>
<?php
}

$abm->generarAbm ("", "Paises");

if (!isset ($_REQUEST['abm_exportar']))
{
	?>
        <p>&nbsp;</p>
        <p>
            <a href='index.php'>Volver al Menu Anterior</a>
        </p>
        <p>&nbsp;</p>
    </div>
</div>
<BR />
<BR />
<BR />
<BR />
<BR />
</body>
</html>
<?php
}

ob_end_flush ();

?>
<!-- <script src="classes/jsABM/jam.stickytableheaders.js"></script> -->
<!-- <script> -->
<!-- 	$("table").stickyTableHeaders(); -->
<!-- </script> -->