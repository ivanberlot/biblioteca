<?php
/**
 * El archivo agrupa las acciones a llevar a cavo sobre los directorio luego de un insert un uppdate o un delete.
 *
 * @author iberlot <@> iberlot@usal.edu.ar
 * @since 19 mar. 2018
 *       @lenguage PHP
 * @name renameFold.php
 * @version 0.1 version inicial del archivo.
 */

/*
 * Querido programador:
 * Cuando escribi este codigo, solo Dios y yo sabiamos como funcionaba.
 * Ahora, Solo Dios lo sabe!!!
 * Asi que, si esta tratando de 'optimizar' esta rutina y fracasa (seguramente),
 * por favor, incremente el siguiente contador como una advertencia para el
 * siguiente colega:
 * totalHorasPerdidasAqui = 10
 */
ob_start ();
require_once ("config/includes.php");

if (isset ($_REQUEST ['tipo']) and $_REQUEST ['tipo'] == 'autor')
{
	if (isset ($_REQUEST ['id']) and $_REQUEST ['id'] != "")
	{
		$carpeta = getArrayPosNobresAutor ($db, $_REQUEST ['id']);

		if (isset ($_REQUEST ['accion']) and $_REQUEST ['accion'] == 2)
		{
			corregirEstructuraAutor ($carpeta);
		}
		elseif (isset ($_REQUEST ['accion']) and $_REQUEST ['accion'] == 1)
		{
			$listado = scandir ('Biblioteca');

			if (! in_array ($carpeta [0], $listado))
			{
				mkdir ('Biblioteca/' . $carpeta [0], 0777, true);
				corregirEstructuraAutor ($carpeta);
				echo $carpeta [0];
			}
		}
		elseif (isset ($_REQUEST ['accion']) and $_REQUEST ['accion'] == 3)
		{

			$listado = scandir ('Biblioteca');

			if (in_array ($carpeta [0], $listado))
			{
				rmdir ('Biblioteca/' . $carpeta [0]);
				echo $carpeta [0];
			}
		}

		header ("Location: /Mytthos/abmAutores.php");
	}
	else
	{
		throw new Exception ('ERROR: Debe pasar un id de autor.');
	}
}
elseif (isset ($_REQUEST ['tipo']) and $_REQUEST ['tipo'] == 'saga')
{
	$where = array ();
	$parametros = array ();

	$where [] = " idSaga  = :idSaga  ";
	$parametros [] = $_REQUEST ['id'];

	if ($where != "")
	{
		$where = implode (" AND ", $where);

		$where = " AND " . $where;
	}

	$sql = "SELECT * FROM Saga WHERE 1=1 " . $where;

	if ($result = $db->query ($sql, true, $parametros))
	{
		if ($rst = $db->fetch_array ($result))
		{
			$rst = array_change_key_case ($rst, CASE_LOWER);

			if (isset ($_REQUEST ['id']) and $_REQUEST ['id'] != "")
			{
				$sql = "SELECT * FROM Autor WHERE 1=1 AND idAutor = " . $rst ['idautor'] . " ";

				$result = $db->query ($sql);
				$datos = $db->fetch_array ($result);
				$datos = array_change_key_case ($datos, CASE_LOWER);

				if (isset ($datos ['segnombre']) and $datos ['segnombre'] != "")
				{
					$autorDir = $datos ['apellido'] . "_" . $datos ['nombre'] . "_" . $datos ['segnombre'];
				}
				else
				{
					$autorDir = $datos ['apellido'] . "_" . $datos ['nombre'];
				}

				$carpeta = $rst ['titulo'];
				$carpeta = trim ($carpeta, " \t\n\r\0\x0B");
				$carpeta = str_replace (" ", "_", $carpeta);

				$listado = scandir ('Biblioteca/' . $autorDir);

				if (isset ($_REQUEST ['accion']) and $_REQUEST ['accion'] == 1)
				{
					if (! in_array ($carpeta, $listado))
					{
						if (mkdir ('Biblioteca/' . $autorDir . '/' . $carpeta, 0777, true))
						{
							print_r ($carpeta);
						}
						else
						{

							exit ("Error en el manejo de archivos<Br>" . 'Biblioteca/' . $autorDir . '/' . $carpeta);
						}
					}
				}
				elseif (isset ($_REQUEST ['accion']) and $_REQUEST ['accion'] == 2)
				{
					$sql = "SELECT titulo FROM T_Saga WHERE ID = (SELECT MAX(ID) FROM T_Saga WHERE idSaga = :idSaga)";
					$parametros = array ();
					$parametros [] = $_REQUEST ['id'];

					$result = $db->query ($sql, $esParam = true, $parametros);
					$datos = $db->fetch_array ($result);
					$datos = array_change_key_case ($datos, CASE_LOWER);

					$carpeta2 = $datos ['titulo'];
					$carpeta2 = trim ($carpeta2, " \t\n\r\0\x0B");
					$carpeta2 = str_replace (" ", "_", $carpeta2);

					if (copia ('Biblioteca/' . $autorDir . '/' . $carpeta2, 'Biblioteca/' . $autorDir . '/' . $carpeta))
					{
						print_r ($carpeta);
					}
					else
					{
						exit ("Error en el manejo de archivos");
					}
				}
				elseif (isset ($_REQUEST ['accion']) and $_REQUEST ['accion'] == 3)
				{
					if (in_array ($carpeta, $listado))
					{
						if (rmdir ('Biblioteca/' . $autorDir . '/' . $carpeta))
						{
							print_r ($carpeta);
						}
						else
						{
							exit ("Error en el manejo de archivos");
						}
					}
				}
			}
		}
	}

	header ("Location: /Mytthos/abmSagas.php?idAutor=" . $rst ['idautor']);
}
elseif (isset ($_REQUEST ['tipo']) and $_REQUEST ['tipo'] == 'libro')
{
	if (isset ($_REQUEST ['id']) and $_REQUEST ['id'] != "")
	{
		$param ['idLibro'] = $_REQUEST ['id'];
		$libro = get_Directorio ($db, $param);

		if (isset ($_REQUEST ['accion']) and $_REQUEST ['accion'] == 1)
		{
			mkdir ('Biblioteca/' . $libro ['directorio'] . '/' . $libro ['archivo'], 0777, true);
			mkdir ('Biblioteca/' . $libro ['directorio'] . '/' . $libro ['archivo'] . '/Capitulos', 0777, true);
			mkdir ('Biblioteca/' . $libro ['directorio'] . '/' . $libro ['archivo'] . '/Audios', 0777, true);
			mkdir ('Biblioteca/' . $libro ['directorio'] . '/' . $libro ['archivo'] . '/Archivos', 0777, true);
		}
		elseif (isset ($_REQUEST ['accion']) and $_REQUEST ['accion'] == 2)
		{
			// UPDATE
			$sql = "SELECT titulo, ordenSaga FROM T_Libro WHERE ID = (SELECT MAX(ID) FROM T_Libro WHERE idLibro = :idLibro)";
			$parametros = array ();
			$parametros [] = $_REQUEST ['id'];

			$result = $db->query ($sql, $esParam = true, $parametros);
			$datos = $db->fetch_array ($result);
			$datos = array_change_key_case ($datos, CASE_LOWER);

			$carpeta2 = $datos ['titulo'];
			$carpeta2 = trim ($carpeta2, " \t\n\r\0\x0B");
			$carpeta2 = str_replace (" ", "_", $carpeta2);
			$carpeta2 = str_pad ($datos ['ordensaga'], 2, "0", STR_PAD_LEFT) . "-" . $carpeta2;

			print_r ('Biblioteca/' . $libro ['directorio'] . '/' . $carpeta2);
			print_r ("<Br>");
			print_r ('Biblioteca/' . $libro ['directorio'] . '/' . $libro ['archivo']);

			if (copia ('Biblioteca/' . $libro ['directorio'] . '/' . $carpeta2, 'Biblioteca/' . $libro ['directorio'] . '/' . $libro ['archivo']))
			{
				print_r ($libro ['directorio']);
			}
			else
			{
				exit ("Error en el manejo de archivos");
			}
		}
		elseif (isset ($_REQUEST ['accion']) and $_REQUEST ['accion'] == 3)
		{
		}
	}

	header ("Location: /Mytthos/abmLibros.php?idSaga=$libro[idSaga]&idAutor=$libro[idAutor]");
}
elseif (isset ($_REQUEST ['tipo']) and $_REQUEST ['tipo'] == 'capitulo')
{
	if (isset ($_REQUEST ['id']) and $_REQUEST ['id'] != "")
	{
		if (isset ($_REQUEST ['accion']) and $_REQUEST ['accion'] == 1)
		{
			$parametros ["idCapitulo"] = $_REQUEST ['id'];

			if ($capitulo = get_Archivo_capitulo ($db, $parametros))
			{
				if (! file_exists ($capitulo ['dirLibro']))
				{
					// FIXME esto debe se remplazado por una funcion que compruebe los directorios de formarecursiva y diga donde esta el error
					throw new Exception ('ERROR: No existe el directorio del libro.');
				}
				(! file_exists ($capitulo ['dirLibro'] . '/Capitulos')) ? mkdir ($capitulo ['dirLibro'] . '/Capitulos', 0777, true) : "";
				(! file_exists ($capitulo ['dirLibro'] . '/Audios')) ? mkdir ($capitulo ['dirLibro'] . '/Audios', 0777, true) : "";
				(! file_exists ($capitulo ['dirLibro'] . '/Archivos')) ? mkdir ($capitulo ['dirLibro'] . '/Archivos', 0777, true) : "";

				if (! file_exists ($capitulo ['archivo']))
				{
					if (! touch ($capitulo ['archivo']))
					{
						throw new Exception ("ERROR creando el archivo: " . $capitulo ['archivo'] . ".");
					}
				}
			}
			else
			{
				exit ("ERROR Buscando el nombre");
			}
		}
		elseif (isset ($_REQUEST ['accion']) and $_REQUEST ['accion'] == 2)
		{
		}
		elseif (isset ($_REQUEST ['accion']) and $_REQUEST ['accion'] == 3)
		{
		}

		$sql = "SELECT idLibro FROM Capitulo WHERE idCapitulo = :idLibro";

		$parametros = array ();
		$parametros [] = $_REQUEST ['id'];

		$result = $db->query ($sql, $esParam = true, $parametros);
		$rst = $db->fetch_array ($result);
		$idLibro = $rst ['idLibro'];
		header ("Location: /Mytthos/abmCapitulos.php?idLibro=$idLibro");
	}
}

?>