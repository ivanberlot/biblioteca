<?php
/**
 * Encargado de mostrar el texto del capitulo.
 *
 * @author iberlot <@> iberlot@usal.edu.ar
 *         @date 23 dic. 2015
 *         @lenguage PHP
 * @name leer.php
 * @version 0.1 version inicial del archivo.
 * @package @project Mytthos
 */
ob_start ();

require_once ("config/includes.php");

$where = array ();
$parametros = array ();
$html = "";

if (isset ($_REQUEST["idCapitulo"]) and $_REQUEST["idCapitulo"] != "")
{
	$where[] = " Capitulo.idCapitulo = :idCapitulo ";
	$parametros[] = trim ($_REQUEST["idCapitulo"]);
}
else
{
	header ("Location:autores.php");

	exit ();
}

if ($where != "")
{
	$where = implode (" AND ", $where);

	$where = " AND " . $where;
}

$sql = "SELECT
					Capitulo.idCapitulo idCapitulo,
					Capitulo.nrOrden nrOrden,
					Capitulo.titulo tituloCap,
					Capitulo.Archivo archivo,
					Capitulo.ArchivoOtro otro,
					Capitulo.codigoYouTube codigoYouTube,
					Libro.idLibro idLibro,
					Libro.ordenSaga ordenSaga,
					Libro.titulo tituloLibro,
					Saga.idSaga idSaga,
					Saga.titulo tituloSaga,
					Autor.idAutor idAutor,
					Autor.apellido apellido,
					Autor.nombre nombre,
					Autor.segNombre segNombre
				FROM
					Capitulo,
					Autor,
					Libro,
					Saga
				WHERE
					Capitulo.idLibro = Libro.idLibro
					AND Libro.idSaga = Saga.idSaga
					AND Saga.idAutor = Autor.idAutor
					" . $where;

if ($result = $db->query ($sql, $esParam = true, $parametros))
{
	if ($row = $db->fetch_array ($result))
	{
		$apellido = trim ($row['apellido']);
		$realname = trim ($row['nombre']);
		$segname = trim ($row['segNombre']);

		if ($segname != "")
		{
			$direCarpeta = $apellido . "_" . $realname . "_" . $segname;
		}
		else
		{
			$direCarpeta = $apellido . "_" . $realname;
		}

		$direCarpeta = str_replace (' ', '_', trim ($direCarpeta));

		$tituloCarpetaSaga = str_replace (' ', '_', trim ($row['tituloSaga']));

		$tituloCarpeta = str_replace (' ', '_', trim ($row['tituloLibro']));

		$tituloLibro = str_pad ($row['ordenSaga'], 2, "0", STR_PAD_LEFT) . "-" . $tituloCarpeta;

		$tituloCarpeta = $direCarpeta . "/" . $tituloCarpetaSaga . "/" . $tituloLibro;

		$capitulo = $row['archivo'];

		// $archivo = "Biblioteca/" . $tituloCarpeta . "/Capitulos/" . $tituloLibro . "-" . $capitulo . ".txt";
		$archivo = "Biblioteca/" . $tituloCarpeta . "/Capitulos/" . $capitulo . ".txt";
		$archivoAudio = "Biblioteca/" . $tituloCarpeta . "/Audios/" . $capitulo . ".mp3";
		// $codigoYouTube = $row['codigoYouTube'];
	}
}

$html .= "<body>";
$html .= "<Div id='cuerpo'>";

// Mostramos el Numero de capitulo y el Titulo
$html .= '<h4>' . $row['nrOrden'] . " - " . $row['tituloCap'] . '</h4>' . "<Br /><Br />";

$ar = fopen ($archivo, "r") or die ("No se pudo abrir el archivo");
$html .= "<div id='capitulo'>";

if ($ar)
{

	$sql2 = "SELECT * FROM Leidos WHERE id_Ususario = :id_Ususario and IdCapitulo = :IdCapitulo";

	$parametros2 = array ();
	$parametros2[] = trim ($_SESSION["id_Ususario"]);
	$parametros2[] = trim ($_REQUEST['idCapitulo']);

	$result2 = $db->query ($sql2, $esParam = true, $parametros2);

	if (!$row2 = $db->fetch_array ($result2))
	{
		$sql3 = "INSERT INTO Leidos(id_Ususario, IdCapitulo) VALUES (:id_Ususario, :IdCapitulo)";
		$parametros3 = array ();
		$parametros3[] = trim ($_SESSION["id_Ususario"]);
		$parametros3[] = trim ($_REQUEST["idCapitulo"]);
		$db->query ($sql3, $esParam = true, $parametros3);
		$db->commit ();
	}
}

$num_lineas = 0;
while (!feof ($ar))
{
	$linea = fgets ($ar); // or die ("No se pudo obtener la linea"); // Obtiene una l&#65533;nea desde el puntero a un fichero
	$lineasalto = nl2br ($linea); // or die ("No se pudo insertar el salto"); // Inserta saltos de l&#65533;nea HTML antes de todas las nuevas l&#65533;neas de un string

	if ($num_lineas > 3)
	{
		if ((mb_detect_encoding ($lineasalto, 'UTF-8', true)) == true)
		{
			$lineasalto = $lineasalto;
		}
		else
		{
			$lineasalto = utf8_encode ($lineasalto);
		}
		$html .= ($lineasalto); // Le indicamos que convierta el texto a utf8 para que reconzca acentos y &#65533;'s
	}

	$num_lineas = $num_lineas + 1;
}
$html .= "</div>";

fclose ($ar);

if (file_exists ($archivoAudio))
{
	$html .= "<audio id='player' src='" . $archivoAudio . "'></audio>
<div>
<button onclick=\"document.getElementById('player').play()\">Play</button>
<button onclick=\"document.getElementById('player').pause()\">Pausa</button>
<button onclick=\"document.getElementById('player').load()\">Stop</button>
<button onclick=\"document.getElementById('player').volume += .1\">Subir volumen</button>
<button onclick=\"document.getElementById('player').volume -= .1\">Bajar volumen</button>
</div> ";
}

// if ($codigoYouTube != "")
// {
// $html .= '<Div align="center"><iframe width="200" height="113" src="https://www.youtube.com/embed/' . $codigoYouTube . '" frameborder="0" allowfullscreen></iframe></Div>';
// }

$sql = "SELECT idCapitulo FROM Capitulo WHERE idLibro = :idlibro AND nrOrden = :orden";

$parametros = array ();
$parametros[0] = $row['idLibro'];
$parametros[1] = $row['nrOrden'] - 1;

$html .= "<Br /><Br /><Br /><Br />";

if ($result = $db->query ($sql, $esParam = true, $parametros))
{
	if ($rows = $db->fetch_array ($result))
	{
		$html .= "<a href='leer.php?idCapitulo=" . $rows['idCapitulo'] . "' accesskey='Left arrow'>Anterior</a>    ";
	}
}

$parametros[1] = $row['nrOrden'] + 1;

if ($result = $db->query ($sql, $esParam = true, $parametros))
{
	if ($rows = $db->fetch_array ($result))
	{
		$html .= "     <a href='leer.php?idCapitulo=" . $rows['idCapitulo'] . "' accesskey='z'>Siguiente</a>";
	}
}

$html .= "<Br /><Br /><Br /><Br /></Div></body>";
echo $html;
?>

</html>
