<?php
/**
 *
 * @author iberlot <@> iberlot@usal.edu.ar
 * @since 22 may. 2018
 * @lenguage PHP
 * @name abmSagas.php
 * @version 0.1 version inicial del archivo.
 */

/*
 * Querido programador:
 *
 * Cuando escribi este codigo, solo Dios y yo sabiamos como funcionaba.
 * Ahora, Solo Dios lo sabe!!!
 *
 * Asi que, si esta tratando de 'optimizar' esta rutina y fracasa (seguramente),
 * por favor, incremente el siguiente contador como una advertencia para el
 * siguiente colega:
 *
 * totalHorasPerdidasAqui = 0
 *
 */
ob_start ();

require_once ("config/includes.php");

$abm = new class_abm ();

// $abm->dbLink = $db_link;
$abm->tabla = "Libro";
$abm->registros_por_pagina = 40;
$abm->campoId = "idLibro";
$abm->campoIdEsEditable = false;
$abm->orderByPorDefecto = "ordenSaga";

$abm->redireccionarDespuesUpdate = "renameFold.php?tipo=libro&accion=2&id=%d";
$abm->redireccionarDespuesInsert = "renameFold.php?tipo=libro&accion=1&id=%d";
$abm->redireccionarDespuesDelete = "renameFold.php?tipo=libro&accion=3&id=%d";

if (isset ($_REQUEST['idAutor']) and $_REQUEST['idAutor'] != "")
{
	$abm->adicionalesSelect = " AND idAutor = " . $_REQUEST['idAutor'] . " ";
	$abm->adicionalesSelect .= " AND idSaga = " . $_REQUEST['idSaga'] . " ";
}

if (isset ($_SESSION['estado']) and $_SESSION['estado'] == 'Iniciada')
{
	$abm->mostrarNuevo = true;
	$abm->mostrarBorrar = true;
	$abm->mostrarEditar = true;
}
else
{
	$abm->mostrarNuevo = false;
	$abm->mostrarBorrar = false;
	$abm->mostrarEditar = false;
}
$abm->busquedaTotal = true;

$abm->campos = array (
		array (
				'campo' => 'idLibro',
				'tipo' => 'texto',
				'exportar' => true,
				'titulo' => 'ID',
				'noEditar' => true,
				'buscar' => true
		),
		array (
				'campo' => 'imagen',
				'tipo' => 'upload',
				'directorio' => 'fotosLibros',
				'alto' => '90',
				'ancho' => '90',
				'cargarEnBase' => TRUE,
				'ubicacionArchivo' => 'fotosLibros',
				'nombreArchivo' => " \$data=str_replace (' ', '_', {{titulo}}).'.jpg';",
				'mostrar' => true,
				'exportar' => true,
				'titulo' => 'FOTO',
				'buscar' => true
		),
		array (
				'campo' => 'titulo',
				'tipo' => 'texto',
				'exportar' => true,
				'titulo' => 'TITULO',
				'buscar' => true
		),
		array (
				'campo' => 'idAutor',
				'tipo' => 'dbCombo',
				'campoValor' => 'idAutor',
				'campoTexto' => 'apellido',
				'joinTable' => 'Autor',
				'joinCondition' => 'LEFT',
				'titulo' => 'Autor',
				'customOrder' => 'apellido',
				'incluirOpcionVacia' => FALSE,
				'noListar' => true,
				'valorPredefinido' => $_REQUEST['idAutor']
		),
		array (
				'campo' => 'idSaga',
				'tipo' => 'dbCombo',
				'campoValor' => 'idSaga',
				'campoTexto' => 'titulo',
				'joinTable' => 'Saga',
				'joinCondition' => 'LEFT',
				'titulo' => 'SAGA',
				'customOrder' => 'titulo',
				'incluirOpcionVacia' => FALSE,
				'noListar' => true,
				'campoOrder' => "ordenSaga",
				'valorPredefinido' => $_REQUEST['idSaga']
		),
		array (
				'campo' => 'resenia',
				'tipo' => 'textarea',
				'tmostrar' => 100,
				'maxLen' => 2000,
				'exportar' => true,
				'titulo' => 'DESCRIPCION',
				'buscar' => true
		),
		array (
				'campo' => 'ordenSaga',
				'cantidadDecimales' => '0',
				'tipo' => 'numero',
				'exportar' => true,
				'titulo' => 'ORDEN',
				'buscar' => true
		),
		array (
				'campo' => 'anio',
				'tipo' => 'numero',
				'cantidadDecimales' => '0',
				'exportar' => true,
				'titulo' => 'AÑO',
				'buscar' => true
		),
		array (
				'campo' => "",
				'tipo' => "texto",
				'titulo' => "Capitulos",
				'maxLen' => 30,
				'customPrintListado' => "<a href='abmCapitulos.php?idLibro={id}' title='Ver Capitulos'>Ver Capitulos</a>",
				'buscar' => false,
				'noEditar' => true,
				'noNuevo' => true
		)
);

if (!isset ($_REQUEST['abm_exportar']))
{
	?>
<!-- Estilos -->
<style>
#cuerpo {
	width: 95%;
}
</style>
<link rel="stylesheet" type="text/css" href="classes/cssABM/abm.css" />
<div id='content'>
    <div id="separadorh"></div>
    <h3 align="center"><?php
	// print $Titulo ?></h3>
    <div id="separadorh"></div>
    <div id='cuerpo' align='center'>
        <div id="separadorh"></div>
<?php
}

$abm->generarAbm ("", "Paises");

if (!isset ($_REQUEST['abm_exportar']))
{
	?>
        <p>&nbsp;</p>
        <p>
            <a href='abmSagas.php?idAutor=<?php

	echo $_REQUEST['idAutor'];
	?>'>Volver al Menu Anterior</a>
        </p>
        <p>&nbsp;</p>
    </div>
</div>
<BR />
<BR />
<BR />
<BR />
<BR />
</body>
</html>
<?php
}

ob_end_flush ();

?>
<!-- <script src="classes/jsABM/jam.stickytableheaders.js"></script> -->
<!-- <script> -->
<!-- 	$("table").stickyTableHeaders(); -->
<!-- </script> -->