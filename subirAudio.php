<?php
/**
 *
 * @author iberlot <@> iberlot@usal.edu.ar
 * @since 13 dic. 2018
 * @lenguage PHP
 * @name subirAudio.php
 * @version 0.1 version inicial del archivo.
 */

/*
 * Querido programador:
 *
 * Cuando escribi este codigo, solo Dios y yo sabiamos como funcionaba.
 * Ahora, Solo Dios lo sabe!!!
 *
 * Asi que, si esta tratando de 'optimizar' esta rutina y fracasa (seguramente),
 * por favor, incremente el siguiente contador como una advertencia para el
 * siguiente colega:
 *
 * totalHorasPerdidasAqui = 0
 *
 */
ob_start ();

require_once ("config/includes.php");

if ($_REQUEST ['idCapitulo'])
{
	$sql = "SELECT * FROM Capitulo WHERE idCapitulo = :idCapitulo ";
	$parametros = array ();
	$parametros [] = trim ($_REQUEST ['idCapitulo']);
	$resultCapitulo = $db->query ($sql, $esParam = true, $parametros);
	$rowCapitulo = $db->fetch_array ($resultCapitulo);

	$libro = Array ();
	$libro ['idLibro'] = trim ($rowCapitulo ['idLibro']);
	$libro = get_Libro ($db, $libro);

	$saga = Array ();
	$saga ['idSaga'] = $libro ['idSaga'];
	$saga = get_Saga ($db, $saga);

	$autor = Array ();
	$autor ['idAutor'] = $libro ['idAutor'];
	$autor = get_Autor ($db, $autor);

	$libro ['titulo'] = trim ($libro ['titulo']);
	$tituloCarpeta = str_replace (' ', '_', $libro ['titulo']);
	$ordenLibro = $libro ['ordenSaga'];

	$saga ['titulo'] = trim ($saga ['titulo']);
	$tituloCarpetaSaga = str_replace (' ', '_', $saga ['titulo']);

	$apellido = trim ($autor ['apellido']);
	$realname = trim ($autor ['nombre']);
	$segname = trim ($autor ['segNombre']);

	if ($segname != "")
	{
		$direCarpeta = $apellido . "_" . $realname . "_" . $segname;
	}
	else
	{
		$direCarpeta = $apellido . "_" . $realname;
	}

	$Carpeta = "Biblioteca/" . $direCarpeta . "/" . $tituloCarpetaSaga . "/" . str_pad ($ordenLibro, 2, "0", STR_PAD_LEFT) . "-" . $tituloCarpeta . "/Audios";
}
else
{
	exit ("Error idCapitulo es un parametro obligatorio");
}

echo "<fieldset><legend>Editar " . $libro ['titulo'] . " - " . $rowCapitulo ['nrOrden'] . " - " . $rowCapitulo ['titulo'] . "</legend>";

if ($_FILES ['archivo'] ["error"] > 0)
{
	echo "Error: " . $_FILES ['archivo'] ['error'] . "<br>";
}
else
{
	/* ahora co la funcion move_uploaded_file lo guardaremos en el destino que queramos */
	move_uploaded_file ($_FILES ['archivo'] ['tmp_name'], $Carpeta . "/" . $rowCapitulo ['Archivo'] . ".mp3");

	echo "Nombre: " . $_FILES ['archivo'] ['name'] . "<br>";
	echo "Tipo: " . $_FILES ['archivo'] ['type'] . "<br>";
	echo "Tamaño: " . ($_FILES ["archivo"] ["size"] / 1024) . " kB<br>";
	echo "Carpeta temporal: " . $_FILES ['archivo'] ['tmp_name'];
}
?>


<!DOCTYPE html>
<html>
<head>
</head>
<body>
	<form action="subirAudio.php" method="post"
		enctype="multipart/form-data">
		<input type="file" name="archivo" id="archivo"></input> <input
			type="hidden" name="idCapitulo" id="idCapitulo"
			value="<?php
			echo $_REQUEST ['idCapitulo'];
			?>"></input> <input type="submit" value="Subir archivo"></input>
	</form>
</body>
</html>